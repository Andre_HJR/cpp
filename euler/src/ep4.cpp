#include <iostream>

int _fun(int num) {
  int x = num, y = 0;
  while (num) {
    y = 10 * y + num % 10;
    num /= 10;
  }
  return x == y ? 1 : 0;
}

int main(const int argc, const char** argv) {
  int ans = 1;
  for (int i = 100; i < 1000; ++i) {
    for (int j = 100; j < 1000; ++j) {
      if (_fun(i * j) && ans < (i * j)) {
	ans = i * j
      }
    }
  }
  std::cout << ans << std::endl;
  return 0;
}
