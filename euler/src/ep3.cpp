#include <iostream>

bool isPrime(long long x){
  for (long long i = 2; i*i < x;++i){
    if (x%i == 0) return false;
  }
  return true;
}

int main(const int argc, const char** argv){
  long long x = 600851475143;
  for (long long i = 2; i<=x; ++i){
    if (0 == x%i && isPrime(x/i)) {
      std::cout << i << '\n' << x/i << std::endl;
      break;	
    }
  }
  return 0;
}
