#include <iostream>

int main(const int argc, const char** argv) {
  for (int i = 1; i < 1000; ++i) {
    for (int j = 1; j < 1000; ++j) {
      int k = 1000 - i - j;
      if (i * i + j * j ==  k * k) {
        std::cout << i * j * k << std::endl;
        // ph: We do not need to add `break` or `return` in for-loop
        //       while the math concept:
        //         Special Pythagorean triplet, which exists exactly
        //       one Pythagorean triplet for which `a + b + c = 1000`.
        //       find the product `a*b*c`.
        break; // stop loop. Doesn't be compact!
      }
    }
  }
  return 0;
}
