#include <iostream>

int main(const int argc, const char** argv) {
  int _1 = 1, _2 = 2;
  int _tmp = 0, sum = 2;
  while (_tmp < 4000000) {
    _tmp = _1 + _2;
    if ( 0 == _tmp % 2 && _tmp < 4000000) sum += _tmp;
    _1 = _2;
    _2 = _tmp;
  }
  std::cout << sum;
  return 0;
}
