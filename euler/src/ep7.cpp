#include <iostream>

inline bool is_prime(const int &number){
    if (number==2||number==3)   return true;
    for (int i=2;i*i<=number;i++)
        if (number%i==0)    return false;
    return true;
}

const int eratos(const int &size,const int target){
    if (target>size)    return -1;
    bool primes[size];
    for (int i=2;i<size;++i)
        primes[i]=true;
    for (int i=2;i<size;++i){
        if (is_prime(i)){
            for (int j=i*2;j<=size;j+=i)
                primes[j]=false;
        }
    }
    int prime_counter=0;
    for (int i=0;i<size;++i){
        if (primes[i]==true)
            prime_counter++;
        if (prime_counter==target)
            return i;
    }
    return 0;
}

int main(const int argc, const char** argv){
    std::cout<<eratos(114514,10001)<<std::endl;
    return 0;
}
