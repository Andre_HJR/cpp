/**
 * @file 0301.cpp
 * @author andre HJR (3191971830@qq.com)
 * @brief 2022-03-01: 未初始化警告
 * @version 0.1
 * @date 2023-05-01
 *
 * @copyright Copyright (c) 2023
 *
 * @details
 *
 *
题目的思路市，判断在左边的某个变量被赋值时，右边变量是否在走遍之前出现过；若没有出现过则计数加1，
 * 最后输出计数即可。
 *  <center><strong>关于优化建议</strong></center>
 * 1.题中指针i由于必须从左边遍历至少一次才能实时更新未初始化的计数，因此考虑从j指针的移动开始优化。
 * 2.由于每次在变量赋值以后左边变量 i - 1 位置及以前都可以排除未初始化的情况了，
 * 因此只需要在每一次 i 指针移动到 i + 1以后统计第 i 个位置的赋值变化即可。
 * 3.首变量的判断可以压缩到后面循环中一起进行。
 * 综上可以考虑开辟数组记录状态。
 * @code {.c++ }
 * const int N = 100010;
 *
 * int n, k, res;
 * int l[N], r[N];
 *
 * int main()
 * {
 *     cin >> n >> k;
 *     for (int i = 1; i <= k; i ++ ) scanf("%d%d", &l[i], &r[i]);
 *     if (r[1]) res ++ ;
 *     for (int i = 2; i <= k; i ++ )
 *     {
 *         bool flag = false;
 *         for (int j = 1; j <= i - 1; j ++ )
 *         {
 *             if (!r[i])
 *             {
 *                 flag = true;
 *                 break;
 *             }
 *             if (r[i] == l[j])
 *             {
 *                 flag = true;
 *                 break;
 *             }
 *         }
 *         if (!flag) res ++ ;
 *     }
 *     cout << res << endl;
 *     return 0;
 * }
 * @endcode
 *
 */
#include <algorithm>
#include <array>
#include <iostream>
const int N = 1e5 + 10;
std::array<bool, N> st;

int main(const int argc, const char** argv) {
  int n = 0, k = 0, x = 0, y = 0, sum = 0;
  std::cin >> n >> k;
  st[0] = true;
  for (int i = 1; i <= k; x = 0, y = 0, ++i) {
    std::cin >> x >> y;
    if (!st[y]) ++sum;
    st[x] = true;
  }
  std::cout << sum;
}