#include <algorithm>
#include <array>
#include <iostream>
const int N = 2e5 + 1;

std::array<int, N> num;
int main() {
  int n = 0, k = 0, m = 0, t = 0, c = 0, q = 0;
  std::cin >> n >> m >> k;
  for (int i = 0; i < n; t = 0, c = 0, ++i)
    std::cin >> t >> c, num[std::max(1, t - (c - 1) - k)]++,
        num[std::max(1, t - k + 1)]--;
  for (int i = 1; i < N; ++i) num[i] += num[i - 1];
  for (int i = 0; i < m; q = 0, ++i) std::cin >> q, std::cout << num[q] << '\n';
  return 0;
}