#include <array>
#include <iostream>
#include <map>
#include <set>
#include <vector>

struct Node {
  int u, v;
  long long x;
  Node() {}
  Node(int a, int b, long long c) : u(a), v(b), x(c) {}
};
struct cmp {
  bool operator()(const std::pair<int, long long> &lhs,
                  const std::pair<int, long long> &rhs) const {
    if (lhs.second == rhs.second) return lhs.first < rhs.first;
    return lhs.second > rhs.second;
  }
};

void _fast_scan(int &number) {
  bool negative = false;
  int c = 0;
  number = 0;
  c = getchar();
  if ('-' == c) negative = true, c = getchar();
  for (; c > 47 && c < 58; c = getchar()) number = number * 10 + c - 48;
  if (negative) number *= -1;
}

std::array<std::set<std::pair<int, long long>, cmp>, 100010> S;
std::array<std::vector<Node>, 100010> A;
std::array<long long, 400010> C;
std::map<std::pair<int, int>, int> M;

int main(const int argc, const char **argv) {
  int tot = 0, n = 0, m = 0, cnt0 = 0, cnt1 = 0;  // cnt0, cnt1通信孤岛，通信对
  std::ios_base::sync_with_stdio(
      false);  // required. if u do not release the io bind, use _fast_scan
               // instead of std::cin.
  std::cin.tie(nullptr);
  std::cin >> n >> m;
  cnt0 = n;
  for (int i = 1; i <= m; ++i) {
    int k = 0;
    std::cin >> k;
    while (k--) {
      int u = 0, v = 0, x = 0, y = 0;
      std::cin >> u >> v >> x >> y;
      A[i].push_back(Node(u, v, x));
      if (i + y <= m) A[i + y].push_back(Node(u, v, -x));
    }
    for (auto p : A[i]) {
      int u = p.u, v = p.v;
      int u1 = 0, v1 = 0, u2 = 0, v2 = 0;  // 对应的
      int x1 = 0, y1 = 0, x2 = 0, y2 = 0;  // 对应的对应的
      long long x = p.x;
      if (!M.count(std::make_pair(u, v))) M[std::make_pair(u, v)] = ++tot;
      if (!M.count(std::make_pair(v, u))) M[std::make_pair(v, u)] = ++tot;
      int t1 = M[std::make_pair(u, v)], t2 = M[std::make_pair(v, u)];
      int s1 = S[u].size(), s2 = S[v].size();
      if (s1) {
        u1 = S[u].begin()->first;
        if (S[u1].size()) x1 = S[u1].begin()->first;
      }
      if (s2) {
        v1 = S[v].begin()->first;
        if (S[v1].size()) y1 = S[v1].begin()->first;
      }
      auto it = S[u].find(std::make_pair(v, C[t1]));
      if (it == S[u].end()) {
        S[u].insert(std::make_pair(v, x));
        C[t1] = x;
      } else {
        C[t1] += x;
        S[u].erase(it);
        if (C[t1]) S[u].insert(std::make_pair(v, C[t1]));
      }
      it = S[v].find(std::make_pair(u, C[t2]));
      if (it == S[v].end()) {
        S[v].insert(std::make_pair(u, x));
        C[t2] = x;
      } else {
        C[t2] += x;
        S[v].erase(it);
        if (C[t2]) S[v].insert(std::make_pair(u, C[t2]));
      }
      int s11 = S[u].size(), s22 = S[v].size();
      if (s1 == 0 && s11) --cnt0;
      if (s1 && s11 == 0) ++cnt0;
      if (s2 == 0 && s22) --cnt0;
      if (s2 && s22 == 0) ++cnt0;
      if (s11) {
        u2 = S[u].begin()->first;
        if (S[u2].size()) x2 = S[u2].begin()->first;
      }
      if (s22) {
        v2 = S[v].begin()->first;
        if (S[v2].size()) y2 = S[v2].begin()->first;
      }
      if (u1 == v && v1 == u) {
        if (u2 == v && v2 == u)
          continue;
        else
          --cnt1;
        if (u == x2) ++cnt1;
        if (v == y2) ++cnt1;
      } else if (u2 == v && v2 == u) {
        ++cnt1;
        if (u == x1) --cnt1;
        if (v == y1) --cnt1;
      } else {
        if (u == x1 && u != x2) --cnt1;
        if (u != x1 && u == x2) ++cnt1;
        if (v == y1 && v != y2) --cnt1;
        if (v != y1 && v == y2) ++cnt1;
      }
    }
    int l = 0;
    std::cin >> l;
    while (l--) {
      int id = 0;
      std::cin >> id;
      if (!S[id].size())
        std::cout << "0\n";
      else
        std::cout << S[id].begin()->first << '\n';
    }
    int p0 = 0, p1 = 0;
    std::cin >> p0 >> p1;
    if (p0) std::cout << cnt0 << '\n';
    if (p1) std::cout << cnt1 << '\n';
  }
}