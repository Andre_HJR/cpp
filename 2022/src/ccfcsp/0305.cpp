#include <array>
#include <iostream>

std::array<int, 100001> arr;
int func1(int n) {
  int ans = 0, mid = n / 2 + 1;
  for (int i = 1; i <= mid; i++)  // 最开始的数段和
    ans = ans + arr[i];
  int temp = ans;
  for (int i = mid + 1; i <= n; i++)
    temp = temp + arr[i] - arr[i - mid],
    ans = std::min(ans, temp);  // 不断往后，不断比较迭代，选出最小的
  return ans;
}
bool check(int x, int n)  // x的判断
{
  int sum = 0, num = 0;
  for (int i = 1; i <= n; i++) {
    sum = sum + arr[i];
    if (sum >= x) sum = 0, num++;
  }
  return num > n / 2;  // 当个数大于n/2时，小c才不会扔完，因为小c只能操作n/2次
}
int func2(int n) {
  int r = 1e9, mid = r / 2;
  int ans = 0;
  while (mid <= r)  // 用二分法不断逼近刚好能达到check条件的x
  {
    if (check(mid, n)) {
      ans = mid;
      if (mid == r) return ans;  // 如果相等且满足，说明最大值r（mid）就是ans
      mid = (mid + 1 + r) / 2;
    } else {
      if (mid == r)
        break;  // 如果相等且不满足，说明ans就是上一次的mid，直接退出
      r = mid - 1;
      mid = r / 2;
    }
  }
  return ans;
}
int main(const int argc, const char** argv) {
  int n = 0, k = 0, tmp = 0;
  std::cin >> n >> k;
  for (int i = 1; i <= n; tmp = 0, i++) std::cin >> tmp, arr[i] = tmp;
  int ans = 0;
  if (((n % 2 == 0) && k == 1) || (n % 2 == 1) && k == 0)
    ans = func1(n);  // 小z面对偶数局面
  else
    ans = func2(n);  // 小z面对奇数局面
  std::cout << ans;
  return 0;
}