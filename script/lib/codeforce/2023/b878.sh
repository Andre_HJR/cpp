codeforce_878_div3() {
    echo "878Div3========================================start"
    echo "878Div3: g1 and g2 are interactive problems"
    for i in {a..f}
    do
        echo "Question codeforce 878 div3".$i
        perl script/judge test 2023 878/div3_$i codeforce
    done
    perl script/judge clean
    echo "878Div3========================================end"
}
codeforce_878_main() {
    for i in 3
    do
        codeforce_878_div$i
    done
}