codeforce_153_div2() {
    echo "153Div2========================================start"
    for i in {a..f}
    do
        echo "Question codeforce 153 div2".$i
        perl script/judge test 2023 153/div2_$i codeforce
    done
    perl script/judge clean
    echo "153Div2========================================end"
}

codeforce_153_main() {
    for i in 2
    do
        codeforce_153_div$i
    done
}
