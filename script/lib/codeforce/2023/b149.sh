codeforce_149_div2() {
    echo "149Div2========================================start"
    for i in {a..f}
    do
        echo "Question codeforce 149 div2".$i
        perl script/judge test 2023 149/div2_$i codeforce
    done
    perl script/judge clean
    echo "149Div2========================================end"
}

codeforce_149_main() {
    for i in 2
    do
        codeforce_149_div$i
    done
}
