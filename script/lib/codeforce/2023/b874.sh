codeforce_874_div3() {
    echo "874Div4========================================start"
    for i in {a..g}
    do
        echo "Question codeforce 874 div3".$i
        perl script/judge test 2023 874/div3_$i codeforce
    done
    perl script/judge clean
    echo "874Div3========================================end"
}
codeforce_874_main() {
    for i in 3
    do
        codeforce_874_div$i
    done
}