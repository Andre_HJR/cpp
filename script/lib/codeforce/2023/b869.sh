codeforce_869_div1() {
    echo "869Div1========================================start"
    for i in {a..f}
    do
        echo "Question codeforce 869 div1".$i
        perl script/judge test 2023 869/div1_$i codeforce
    done
    perl script/judge clean
    echo "869Div1========================================end"
}
codeforce_869_div2() {
    echo "869Div2========================================start"
    for i in {a..f}
    do
        echo "Question codeforce 869 div2".$i
        perl script/judge test 2023 869/div2_$i codeforce
    done
    perl script/judge clean
    echo "869Div2========================================end"
}

codeforce_869_main() {
    for i in {1..2}
    do
        codeforce_869_div$i
    done
}