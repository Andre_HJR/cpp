ccfcsp_202303() {
    echo "CCFCSP2023-03========================================start"
    for i in {1..5}
    do
        echo "Question ccfcsp".$i
        perl script/judge test 2023 030$i ccfcsp
    done
    perl script/judge clean
    echo "CCFCSP2023-03========================================end"
}
