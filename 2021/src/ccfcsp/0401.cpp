#include <iostream>
#include <array>

int main(const int argc, const char** argv)
{
    enum { COLOR = 256 };
	int L = 0, m = 0, n = 0, tmp = 0;
    std::cin >> m >> n >> L;
    std::array<int, COLOR> h {0};
    // for (int i = 0; i < n; ++i) for (int j = 0; j < m; ++j)
    for (int i = 0; i < m * n; ++i)
	{
        std::cin >> tmp;
		++h[tmp];
	}
	for (int i = 0; i< L; i++) std::cout << h[i] << " ";
	return 0;
}