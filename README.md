# README

> cpp code for COMPETITION.


## Tips


### Simple Test Framework

```python
import os

if __name__ == '__main__':
    os.system("g++ main.cpp")
    os.system("a.exe < in.txt > _out.txt")
    os.system("fc _out.txt out.txt") # in Win
```

### I/O Faster

```cpp
void fastscan(int &number)
{
    //variable to indicate sign of input number
    bool negative = false;
    register int c;
  
    number = 0;
  
    // extract current character from buffer
    c = getchar();
    if (c=='-')
    {
        // number is negative
        negative = true;
  
        // extract the next character from the buffer
        c = getchar();
    }
  
    // Keep on extracting characters if they are integers
    // i.e ASCII Value lies from '0'(48) to '9' (57)
    for (; (c>47 && c<58); c=getchar())
        number = number *10 + c - 48;
  
    // if scanned input has a negative sign, negate the
    // value of the input number
    if (negative)
        number *= -1;
}
```