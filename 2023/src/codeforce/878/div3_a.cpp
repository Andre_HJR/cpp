
#include <algorithm>
#include <iostream>
#include <iterator>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0;
    std::cin >> n;
    std::string s;
    std::cin >> s;
    std::string ans = "";
    while (s.size()) {
      char tt = s.back();
      s.pop_back();
      while (s.size() && tt != s.back()) s.pop_back();
      s.pop_back();
      ans.push_back(tt);
    }
    std::copy(ans.rbegin(), ans.rend(),
              std::ostreambuf_iterator<char>(std::cout));
    std::cout << '\n';
  } while (--t);
  return 0;
}