#include <iostream>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0, k = 0, q = 0, i = 0;
    std::cin >> n >> k >> q;
    std::vector<int> a(n, 0);
    for (i = 0; i < n; ++i) std::cin >> a[i];
    long long ans = 0;
    int lst = -1;
    for (i = 0; i < n; ++i) {
      if (a[i] > q) lst = i;
      ans += std::max(0, i - lst - k + 1);
    }
    std::cout << ans << '\n';
  } while (--t);
  return 0;
}