#include <iostream>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    long long n = 0ll, k = 0ll;
    std::cin >> n >> k;
    k = std::min(k, 40ll);
    long long ans = std::min(n, (1ll << k) - 1) + 1;
    std::cout << ans << '\n';
  } while (--t);
  return 0;
}