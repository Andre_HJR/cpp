#include <algorithm>
#include <iostream>
#include <queue>
#include <set>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    std::string S[2];
    std::cin >> S[0] >> S[1];
    std::set<int> ne;
    int n = static_cast<int>(S[0].size()), i = 0, t = 0, q = 0;
    for (i = 0; i < n; ++i) {
      if (S[0][i] != S[1][i]) ne.insert(i);
    }
    std::cin >> t >> q;
    std::queue<std::pair<int, int>> tbu;
    for (i = 0; i < q; ++i) {
      while (static_cast<int>(tbu.size()) && i == tbu.front().first) {
        int v = tbu.front().second;
        tbu.pop();
        if (S[0][v] != S[1][v]) {
          ne.insert(v);
        }
      }
      int ty = 0;
      std::cin >> ty;
      switch (ty) {
        case 1: {
          int p = 0;
          std::cin >> p;
          --p;
          tbu.push({i + t, p});
          ne.erase(p);
        } break;
        case 2: {
          int s1 = 0, s2 = 0, p1 = 0, p2 = 0;
          std::cin >> s1 >> p1 >> s2 >> p2;
          --s1, --s2, --p1, --p2;
          std::swap(S[s1][p1], S[s2][p2]);
          std::vector<int> vals{p1, p2};
          std::for_each(vals.begin(), vals.end(), [&](auto& i) {
            ne.erase(i);
            if (S[0][i] != S[1][i]) ne.insert(i);
          });
        } break;
        default:
          std::cout << (0 == static_cast<int>(ne.size()) ? "Yes" : "No")
                    << '\n';
      }
    }
  } while (--t);
  return 0;
}