#include <algorithm>
#include <iostream>
#include <vector>

const long long MOD = 1000000007;

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0, i = 0;
    std::cin >> n;
    std::vector<long long> a(n, 0);
    for (i = 0; i < n; ++i) std::cin >> a[i];
    std::sort(a.begin(), a.end());
    long long low = 0, mid = 0, height = 2e9;
    while (low < height) {
      mid = (low + height) / 2;
      if ([&](long long mid) -> bool {
            long long cur = -MOD;
            int used = 0, i = 0;
            for (i = 0; i < n; ++i) {
              if (std::abs(a[i] - cur) > mid) {
                cur = a[i] + mid;
                ++used;
              }
            }
            return used <= 3;
          }(mid))
        height = mid;
      else
        low = mid + 1;
    }
    std::cout << low << '\n';
  } while (--t);
  return 0;
}