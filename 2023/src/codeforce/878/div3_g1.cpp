#include <bits/stdc++.h>
using namespace std;

mt19937_64 rng(chrono::steady_clock::now().time_since_epoch().count());

int main() {
  using i64 = long long;
  int mx = 0;

  int _;
  cin >> _;

  auto go = [](int x) {
    cout << "+ " << x << endl;
    cin >> x;
    return x;
  };

  auto guess = [](i64 x) {
    cout << "! " << x << endl;
    exit(0);
  };

  int last;
  for (int i = 1; i <= 99; i += 1) {
    int x = rng() % 1000000000 + 1;
    last = go(x);
    mx = max(mx, last);
  };
  map<int, i64> dp;
  dp[last] = 0;
  i64 dist = 0;
  for (int i = 1; i <= 450; i += 1) {
    last = go(1);
    dist += 1;
    if (dp.find(last) != dp.end()) {
      guess(dist - dp[last]);
    }
    dp[last] = dist;
  }
  if (mx) {
    last = go(mx);
    dist += mx;
    if (dp.find(last) != dp.end()) {
      guess(dist - dp[last]);
    }
    dp[last] = dist;
  }
  for (int i = 1; i <= 450; i += 1) {
    last = go(450);
    dist += 450;
    if (dp.find(last) != dp.end()) {
      guess(dist - dp[last]);
    }
    dp[last] = dist;
  }
  return 0;
}