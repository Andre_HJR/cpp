#include <algorithm>
#include <iostream>
#include <vector>

const int MOD = 1000000007;

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int N, M;
    std::cin >> N >> M;
    bool vis[N + 1][M + 1];
    for (int i = 0; i < N + 1; ++i)
      for (int j = 0; j < M + 1; ++j) vis[i][j] = false;
    vis[0][0] = true;
    int K;
    std::cin >> K;
    std::vector<std::pair<int, std::pair<int, int>>> vals(K);
    for (int i = 0; i < K; ++i)
      std::cin >> vals[i].first >> vals[i].second.second >>
          vals[i].second.first;
    std::sort(vals.begin(), vals.end());
    int p = 0;
    for (int t = 1; t < MOD; ++t) {
      bool ok = false;
      for (int i = N; i >= 0; --i) {
        for (int j = M; j >= 0; --j) {
          if (!vis[i][j]) continue;
          ok = true;
          if (i < N) vis[i + 1][j] = true;
          if (j < M) vis[i][j + 1] = true;
        }
      }
      if (!ok) break;
      while (p < K && vals[p].first == t) {
        int c = vals[p].second.first;
        if (vals[p].second.second == 1) {
          for (int i = 0; i < M + 1; ++i) {
            vis[c][i] = false;
          }
        } else {
          for (int i = 0; i < N + 1; ++i) {
            vis[i][c] = false;
          }
        }
        ++p;
      }
      if (vis[N][M]) {
        std::cout << t << '\n';
        goto _RET;
      }
    }
    std::cout << -1 << '\n';
  _RET:;
  } while (--t);
  return 0;
}