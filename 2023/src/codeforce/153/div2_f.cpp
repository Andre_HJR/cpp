// #include <bits/stdc++.h>
#include <iostream>
#include <vector>
#include <queue>
#include <numeric>
#include <cmath>
#include <cassert>
#include <algorithm>
#include <map>

#define F first
#define S second
#define all(x) (x).begin(), (x).end()
#define rall(x) (x).rbegin(), (x).rend()
#define sz(x) ((int)(x).size())

using namespace std;
using ll = long long;
using pi = pair<int, int>;
using vi = vector<int>;

template<class T> bool ckmin(T& a, const T& b) { return b < a ? a = b, true : false; }
template<class T> bool ckmax(T& a, const T& b) { return a < b ? a = b, true : false; }

template<class T> using pq_min = priority_queue<T, vector<T>, greater<T>>;
template<class T> using pq_max = priority_queue<T>;

struct SegmentTree {
    int n;
    vector<ll> tadd, tmin;

    SegmentTree(int n) : n(n), tadd(4*n), tmin(4*n) { }

    void add(int v, int vl, int vr, int l, int r, int dx) {
        if (l <= vl && vr <= r) {
            tmin[v] += dx;
            tadd[v] += dx;
        } else if (vr <= l || r <= vl) {
            // nope :D
        } else {
            const int vm = (vl + vr) / 2;
            add(2*v, vl, vm, l, r, dx);
            add(2*v+1, vm, vr, l, r, dx);
            tmin[v] = tadd[v] + ::min(tmin[2*v], tmin[2*v+1]);
        }
    }

    void add(int l, int r, int dx) {
        add(1, 0, n, l, r, dx);
    }

    int min() {
        return tmin[1];
    }
};

template<typename T>
int get_pos(const vector<T>& a, const T& x) {
    return lower_bound(a.begin(), a.end(), x) - a.begin();
}

void solve() {
    int n0, n = 0; cin >> n0; n0 *= 2;
    map<pi, int> was;
    vector<int> a, b, c;
    for (int i = 0; i < n0; ++i) {
        int pa, pb; string pc;
        cin >> pa >> pb >> pc;
        if (!was.count({pa, pb})) {
            was[{pa, pb}] = n++;
            a.push_back(pa);
            b.push_back(pb);
            c.push_back(0);
        }

        const int j = was[{pa, pb}];
        if (pc[0] == '(') c[j]++;
        if (pc[0] == ')') c[j]--;
    }

    int sum_c = 0;
    for (int x : c) sum_c += x;
    assert(sum_c == 0);
    
    bool ans = false;
    vector<int> ord(n), pos(n);
    iota(ord.begin(), ord.end(), 0);
    sort(ord.begin(), ord.end(), [&](int i, int j) {
        return make_pair(a[i], b[i]) < make_pair(a[j], b[j]);
    });

    SegmentTree sgt(n);
    for (int i = 0; i < n; ++i) pos[ord[i]] = i;
    for (int i = 0; i < n; ++i) sgt.add(pos[i], n, c[i]);
    if (sgt.min() >= 0) ans = true;

    vector<pair<int, int>> collisions;
    for (int i = 0; i < n; ++i)
    for (int j = 0; j < i; ++j) {
        if (a[i] <= a[j] && b[i] <= b[j]) continue;
        if (a[j] <= a[i] && b[j] <= b[i]) continue;
        collisions.emplace_back(i, j);
    }

#define make_ratio(p, q, x) ll p = abs(a[(x).F] - a[(x).S]), q = abs(b[(x).F] - b[(x).S]);
    sort(collisions.begin(), collisions.end(), [&](pi x1, pi x2) {
        make_ratio(p1, q1, x1);
        make_ratio(p2, q2, x2);
        return (ll) p1 * q2 < (ll) p2 * q1;
    });

    for (int _i = 0; _i < collisions.size() && !ans; ) {
        int _j = _i++;

        while (_i != collisions.size()) {
            make_ratio(p1, q1, collisions[_j]);
            make_ratio(p2, q2, collisions[_i]);
            if ((ll) p1 * q2 < (ll) p2 * q1) break;
            ++_i;
        }

        vector<int> guys;
        for (int i = _j; i < _i; ++i) {
            guys.push_back(collisions[i].F);
            guys.push_back(collisions[i].S);
        }
        sort(guys.begin(), guys.end());
        guys.erase(unique(guys.begin(), guys.end()), guys.end());

        make_ratio(Q, P, collisions[_j]);
        stable_sort(guys.begin(), guys.end(), [&](int l, int r) {
            return make_pair(a[l]*P + b[l]*Q, b[l]) < make_pair(a[r]*P + b[r]*Q, b[r]);
        });

        vector<int> gpos;
        vector<ll> func;
        for (int i : guys) gpos.push_back(pos[i]);
        for (int i : guys) func.push_back(a[i]*P+b[i]*Q);
        sort(gpos.begin(), gpos.end());

        for (int i : guys) {
            sgt.add(pos[i], n, -c[i]);
            pos[i] = gpos[get_pos(func, a[i]*P+b[i]*Q)];
            sgt.add(pos[i], n, +c[i]);
        }

        if (sgt.min() >= 0) {
            ans = true;
        }

        for (int j = 0; j < guys.size(); ++j) {
            int i = guys[j]; sgt.add(pos[i], n, -c[i]);
            pos[i] = gpos[j];
            ord[pos[i]] = i;
            sgt.add(pos[i], n, +c[i]);
        }
    }

    cout << (ans ? "YES" : "NO") << '\n';
}

int main() {
    ios::sync_with_stdio(0); cin.tie(0);

    int t = 1;
    cin >> t;
    while (t--) solve();
}