#include <iostream>
#include <vector>

// TODO: 不出意外这是一个博弈论
int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0; std::cin >> n;
    std::vector<int> a(n + 1);
    for (int i = 0; i < n; ++i) {
        std::cin >> a[i];
    }
    // Note: a: 2 1 4 3
    int sg1 = n + 1, ans = 0, sg0 = n + 1;
    for (int i = 0; i < n; ++i) {
        if (a[i] < sg1) {
            sg1 = a[i];
        } else if (a[i] < sg0) {
            sg0 = a[i];
            ++ans;
        }
    }
    std::cout << ans << std::endl;
  } while (--t);
  return 0;
}