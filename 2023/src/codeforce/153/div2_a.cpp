#include <iostream>
#include <string>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    std::string s; std::cin >> s;
    if ("()" == s) {
        std::cout << "NO\n";
        continue;
    }
    std::string a, b;
    for(int i = 0; i < 2 * s.size(); ++i) {
        a.push_back((i % 2) ? ')' : '(');
        b.push_back((i < s.size()) ? '(' : ')');
    }
    int ab = 0, bb = 0;
    for (int i = 0; i < s.size(); ++i) {
        if (s == a.substr(i, s.size())) {
            ab = 1;
        }
        if (s == b.substr(i, s.size())) {
            bb = 1;
        }
    }
    std::cout << "YES\n" << (!ab ? a : b) << std::endl;
  } while (--t);
  return 0;
}