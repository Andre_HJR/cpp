#include <iostream>

// TODO: Confused By Question
int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int m, k, a1, ak;
    std::cin >> m >> k >> a1 >> ak;
    int t = std::max(0, std::min(m / k, (m - a1 + k - 1) / k));
    int ans = std::max(0, t - ak) + std::max(0, m - t * k -a1);
    std::cout << ans << std::endl;
  } while (--t);
  return 0;
}