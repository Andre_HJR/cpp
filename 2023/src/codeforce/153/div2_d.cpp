#include <iostream>
#include <string>
#include <limits>
#include <vector>
#define minize(a,b) do {if( (b) < (a) ) { (a) = (b);} } while(0);
const int
        MAX=105,
        D = MAX * MAX,
        INF=std::numeric_limits<int>::max();
 
int main(const int argc, const char** argv) {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);
    std::string S; std::cin>>S;
    std::vector<std::vector<std::vector<int>>> dp(2, std::vector<std::vector<int>>(MAX, std::vector<int>(MAX*MAX*2)));
    int N=S.size();
    for(int i=0;i<2;i++)
        for(int j=0;j<=N;j++)
            for(int k=0;k<MAX*MAX*2;k++)
                dp[i][j][k]=INF;

    dp[0][0][D]=0;

    int al=0;
    for(int i=0;i<N;i++){
        int s=i&1,t=s^1;
        if(S[i]=='0') al++;
        for(int j=0;j<=i;j++){
            for(int k=0;k<MAX*MAX*2;k++) {
                if(dp[s][j][k]==INF) continue;
                minize(dp[t][j+1][k-(i-j)],dp[s][j][k]+(S[i]=='1'));
                minize(dp[t][j][k+j],dp[s][j][k]+(S[i]=='0'));
                
                dp[s][j][k]=INF;
            }
        }
    }

    std::cout << dp[N&1][al][D]/2 << std::endl;
    return 0;
}