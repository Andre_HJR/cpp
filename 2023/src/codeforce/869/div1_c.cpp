#include <bits/stdc++.h>
using namespace std;

const int MOD = 1e9 + 7;
const int N = 3e6;
int fact[N];
int invfact[N];

int binom(int n, int k) {
  if (k < 0 || k > n) return 0;
  return fact[n] * (1LL * invfact[k] * invfact[n - k] % MOD) % MOD;
}

int inversemod(int p, int q) {
  // assumes p > 0
  // https://codeforces.com/blog/entry/23365
  return (p > 1 ? q - 1LL * inversemod(q % p, p) * q / p : 1);
}

pair<int, int> calc(vector<int>& a) {
  pair<int, int> ans;
  int n = (int)a.size() - 1;
  for (int i = 0; i < n; i++) {
    ans.first =
        (ans.first + (i & 1 ? -1LL : 1LL) * binom(n - 1, i) * a[i]) % MOD;
    ans.second =
        (ans.second + (i & 1 ? -1LL : 1LL) * binom(n - 1, i) * a[i + 1]) % MOD;
  }
  return ans;
}

int main(const int argc, const char** argv) {
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  fact[0] = 1;
  for (int i = 1; i < N; i++) fact[i] = 1LL * i * fact[i - 1] % MOD;
  invfact[N - 1] = inversemod(fact[N - 1], MOD);
  for (int i = N - 1; i >= 1; i--) invfact[i - 1] = 1LL * i * invfact[i] % MOD;
  int n;
  cin >> n;
  vector<int> a(n + 1), b(n + 1);
  for (int& x : a) cin >> x;
  for (int& x : b) cin >> x;
  // auto [a0, a1] = calc(a);
  auto ap = calc(a);
  // auto [b0, b1] = calc(b);
  auto bp = calc(b);
  int slope = (ap.second - ap.first) % MOD;
  if (slope < 0) slope += MOD;
  int ans = 1LL * (bp.first - ap.first) * inversemod(slope, MOD) % MOD;
  if (ans < 0) ans += MOD;
  cout << ans << '\n';
}