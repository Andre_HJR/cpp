#include <bits/stdc++.h>
using namespace std;

const int MOD = 1e9 + 7;

int main() {
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  int T;
  cin >> T;
  while (T--) {
    int n;
    cin >> n;
    vector<int> a(n);
    for (int& x : a) cin >> x;
    sort(a.begin(), a.end());
    vector<pair<int, int>> terms;
    auto get_sign = [&]() {
      sort(terms.begin(), terms.end(), greater<>());
      int eps = 0;
      int cur = 1e9;
      long long sum = 0;
      for (auto& term : terms) {
        if (term.first < cur) {
          while (sum && cur > term.first) {
            if (sum & 1) {
              if (sum > 0)
                eps = 1;
              else if (sum < 0)
                eps = -1;
            }
            sum /= 2;
            cur--;
          }
          cur = term.first;
        }
        sum += term.second;
      }
      if (sum > 0) return 1;
      if (sum < 0) return -1;
      if (eps > 0) return 1;
      if (eps < 0) return -1;
      return 0;
    };

    auto calc = [&](auto&& self, int l, int r) {
      if (l == r) return l;
      int m = (l + r) / 2;
      int x1 = self(self, l, m);
      int x2 = self(self, m + 1, r);
      terms.clear();
      terms.emplace_back(x1, -a[x1]);
      for (int i = x1 + 1; i <= x2 + 1; i++) {
        terms.emplace_back(n - i - (i == x1 + 1), a[i]);
      }
      for (int i = x1; i <= x2; i++) {
        terms.emplace_back(i + 1 - (i == x2), a[i]);
      }
      terms.emplace_back(n - 2 - x2, -a[x2 + 1]);
      // cout << "split " << x1 << ' ' << x2 << endl;
      // for (auto& [e, x]: terms) cout << e << ' ' << x << endl;
      if (get_sign() > 0) return x1;
      return x2;
    };
    int c = calc(calc, 0, n - 2);
    // cout << c << endl;

    int ans = 0;
    int pw = 1;
    for (int i = 0; i <= c; i++) {
      if (i != c) pw = 1LL * pw * (MOD + 1) / 2 % MOD;
      ans = (ans - 1LL * pw * a[i]) % MOD;
    }
    pw = 1;
    for (int i = n - 1; i >= c + 1; i--) {
      if (i != c + 1) pw = 1LL * pw * (MOD + 1) / 2 % MOD;
      ans = (ans + 1LL * pw * a[i]) % MOD;
    }
    if (ans < 0) ans += MOD;
    cout << ans << '\n';
  }
}