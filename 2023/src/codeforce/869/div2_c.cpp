#include <bits/stdc++.h>
using namespace std;

using i64 = long long;
using vll = vector<long long>;
#define rep(i, a, b) for (int i = a; i < b; i++)
#define per(i, a, b) for (int i = a; i >= b; i--)
#define all(x) x.begin(), x.end()
#define rall(x) x.rbegin(), x.rend()
#define ins insert
#define fi first
#define se second
#define pb push_back
#define elif else if
i64 pre[200005];
i64 a[200005];

int main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  cout.tie(nullptr);

  i64 n, q;
  cin >> n >> q;
  rep(i, 1, n + 1) { cin >> a[i]; }
  rep(i, 3, n + 1) {
    pre[i] = pre[i - 1] + (a[i] <= a[i - 1] && a[i - 1] <= a[i - 2]);
  }
  while (q--) {
    int l, r;
    cin >> l >> r;
    if (r - l + 1 <= 2) {
      cout << r - l + 1 << "\n";
      continue;
    }
    cout << r - l + 1 - (pre[r] - pre[l + 1]) << "\n";
  }

  return 0;
}
