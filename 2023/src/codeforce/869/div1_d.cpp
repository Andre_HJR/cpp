#include <bits/stdc++.h>
using namespace std;

int main() {
  ios_base::sync_with_stdio(0);
  cin.tie(0);
  int n, k;
  cin >> n >> k;
  if (k <= n / 2) {
    for (int i = 0; i < n / 4; i++) cout << "LDLU";
    for (int i = 0; i < k - 1; i++) cout << "DRUL";
    cout << '\n';
  } else {
    for (int i = 0; i < n / 4; i++) cout << "RDRU";
    for (int i = 0; i < (k == n - 2 ? n / 2 : n - 2 - k); i++) cout << "DLUR";
    for (int i = 0; i < n / 2; i++) cout << "LDLU";
    cout << "RDL";
    cout << '\n';
  }
}