#include <bits/stdc++.h>
using namespace std;

using i64 = long long;
using vll = vector<long long>;
#define rep(i, a, b) for (int i = a; i < b; i++)
#define per(i, a, b) for (int i = a; i >= b; i--)
#define all(x) x.begin(), x.end()
#define rall(x) x.rbegin(), x.rend()
#define ins insert
#define fi first
#define se second
#define pb push_back
#define elif else if

typedef long long ll;

const int INF = 0x3f3f3f3f;
const ll LINF = 0x3f3f3f3f3f3f3f3fLL;

void milana() {
  i64 n, m;
  cin >> n >> m;

  vector<vector<int>> g(n);
  for (int i = 0; i < m; i++) {
    int a, b;
    cin >> a >> b;
    --a, --b;
    g[a].push_back(b);
    g[b].push_back(a);
  }

  rep(i, 0, n) if (g[i].size() >= 4) {
    vector<int> color(n, -1), parent(n, -1);
    queue<int> q;
    color[i] = 0;
    for (int j = 0; j < g[i].size(); j++) {
      q.push(g[i][j]);
      color[g[i][j]] = j + 1;
      parent[g[i][j]] = i;
    }

    pair<int, int> cycle = {-1, -1};
    while (q.size()) {
      int v = q.front();
      q.pop();
      for (int u : g[v]) {
        if (color[u] == -1) {
          q.push(u);
          color[u] = color[v];
          parent[u] = v;
        } else if (parent[v] != u && color[u] != color[v]) {
          cycle = pair<int, int>(v, u);
        }
      }
    }

    if (cycle.first == -1) continue;

    vector<int> used(n, false);
    vector<pair<int, int>> edges;
    int at = cycle.first;
    while (at != i) {
      edges.emplace_back(at, parent[at]);
      used[at] = true;
      at = parent[at];
    }
    at = cycle.second;
    while (at != i) {
      edges.emplace_back(at, parent[at]);
      used[at] = true;
      at = parent[at];
    }
    edges.push_back(cycle);

    int cnt = 0;
    for (int j : g[i])
      if (not used[j] && cnt++ < 2) edges.emplace_back(i, j);

    cout << "YES\n";
    cout << edges.size() << "\n";
    for (auto [a, b] : edges) cout << a + 1 << " " << b + 1 << "\n";
    return;
  }

  cout << "NO\n";
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  cout.tie(nullptr);

  int TEST = 1;
  cin >> TEST;
  while (TEST-- > 0) {
    milana();
  }

  return 0;
}