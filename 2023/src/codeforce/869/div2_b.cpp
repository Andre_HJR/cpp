#include <bits/stdc++.h>
using namespace std;

using i64 = long long;
using vll = vector<long long>;
#define rep(i, a, b) for (int i = a; i < b; i++)
#define per(i, a, b) for (int i = a; i >= b; i--)
#define all(x) x.begin(), x.end()
#define rall(x) x.rbegin(), x.rend()
#define ins insert
#define fi first
#define se second
#define pb push_back
#define elif else if

void milana() {
  i64 n;
  cin >> n;
  if (n == 1) {
    cout << 1 << "\n";
    return;
  }
  if (n % 2) {
    cout << -1 << "\n";
    return;
  }
  for (int i = 1; i <= n; i++) {
    if (i % 2 == 1)
      cout << i + 1 << " ";
    else
      cout << i - 1 << " ";
  }
  cout << "\n";
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  cout.tie(nullptr);

  int TEST = 1;
  cin >> TEST;
  while (TEST-- > 0) {
    milana();
  }

  return 0;
}