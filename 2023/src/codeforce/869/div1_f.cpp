#include <bits/stdc++.h>
#define L(i, j, k) for (int i = (j); i <= (k); ++i)
#define R(i, j, k) for (int i = (j); i >= (k); --i)
#define ll long long
#define vi vector<int>
#define sz(a) ((int)(a).size())
#define me(f, x) memset(f, x, sizeof(f))
using namespace std;
const int N = 1e6 + 7, mod = 998244353;

int qpow(int x, int y = mod - 2) {
  int res = 1;
  for (; y; x = (ll)x * x % mod, y >>= 1)
    if (y & 1) res = (ll)res * x % mod;
  return res;
}
int fac[N], ifac[N], inv[N];
void init(int x) {
  fac[0] = ifac[0] = inv[1] = 1;
  L(i, 2, x) inv[i] = (ll)(mod - mod / i) * inv[mod % i] % mod;
  L(i, 1, x)
  fac[i] = (ll)fac[i - 1] * i % mod, ifac[i] = (ll)ifac[i - 1] * inv[i] % mod;
}
int C(int x, int y) {
  return x < y || y < 0 ? 0 : (ll)fac[x] * ifac[y] % mod * ifac[x - y] % mod;
}

int n;
int ch[N][26], fa[N], len[N], tot = 1, lst = 1, edp[N], cnt[N];
void cpy(int x, int y) {
  L(i, 0, 25) ch[y][i] = ch[x][i];
  len[y] = len[x], fa[y] = fa[x];
}
void ins(int x) {
  int p = lst, now = lst = ++tot;
  len[now] = len[p] + 1;
  for (; !ch[p][x]; p = fa[p]) ch[p][x] = now;
  if (!p)
    fa[now] = 1;
  else {
    int pto = ch[p][x];
    if (len[pto] == len[p] + 1)
      fa[now] = pto;
    else {
      int sp = ++tot;
      cpy(pto, sp), len[sp] = len[p] + 1;
      fa[pto] = fa[now] = sp;
      for (; ch[p][x] == pto; p = fa[p]) ch[p][x] = sp;
    }
  }
}

unordered_map<ll, int> mp;
int idt, ql[N], qr[N];
int getid(int l, int r) {
  ll u = (ll)l * (n + 1) + r;
  if (mp.count(u)) return mp[u];
  mp[u] = ++idt, ql[idt] = l, qr[idt] = r;
  return idt;
}
vi qwq[N];

int Get(int x, int len) {
  int c = getid(edp[x] - len + 1, edp[x]);
  qwq[cnt[x]].emplace_back(c);
  return c;
}

int q[N], dp[N], nw[N];
char s[N];
vi G[N];
mt19937_64 orz(time(0) ^ clock());

vi sz[N];
void xins(int p, int t) {
  for (int x = p; x; x -= x & -x) sz[x].emplace_back(t);
}
void xcle(int p) {
  for (int x = p; x; x -= x & -x) sz[x].clear();
}

int Len[N], tlen[N], dis[N];

int main() {
  ios ::sync_with_stdio(false);
  cin.tie(0);
  cout.tie(0);
  //	n = 1e5;
  //	L(i, 1, n)
  //		s[i] = i & 1 ? 'a' : 'b';
  cin >> (s + 1), n = strlen(s + 1);
  init(n), me(edp, 0x3f);
  L(i, 1, n) ins(s[i] - 'a'), edp[lst] = i, cnt[lst] = 1;
  L(i, 1, tot) q[i] = i;
  sort(q + 1, q + tot + 1, [&](int x, int y) { return len[x] < len[y]; });
  R(i, tot, 2)
  edp[fa[q[i]]] = min(edp[fa[q[i]]], edp[q[i]]), cnt[fa[q[i]]] += cnt[q[i]];
  edp[1] = 0, cnt[1] = n + 1;
  L(i, 2, tot) {
    int x = q[i];
    assert(cnt[x] != cnt[fa[x]]);
    G[Get(x, len[fa[x]] + 1)].emplace_back(Get(fa[x], len[fa[x]]));
  }
  L(i, 2, tot)
  L(j, 0, 25)
  if (ch[i][j] && cnt[ch[i][j]] != cnt[i])
    L(o, max(len[fa[i]] + 1, len[fa[ch[i][j]]]), min(len[i], len[ch[i][j]] - 1))
  G[Get(ch[i][j], o + 1)].emplace_back(Get(i, o));
  int rt = getid(1, n);
  dp[rt] = 1, qwq[1].emplace_back(rt);
  int nt = 0;
  ll ans = 0;
  L(cnt, 1, n) if (sz(qwq[cnt])) {
    sort(qwq[cnt].begin(), qwq[cnt].end(), [&](int u, int v) {
      return make_pair(ql[u], -qr[u]) < make_pair(ql[v], -qr[v]);
    });
    qwq[cnt].erase(unique(qwq[cnt].begin(), qwq[cnt].end()), qwq[cnt].end());
    L(p, 0, sz(qwq[cnt]) - 1) {
      int np = p;
      while (np < sz(qwq[cnt]) - 1 && qr[qwq[cnt][np + 1]] <= qr[qwq[cnt][p]]) {
        ++np;
      }
      int mnl = 1e9, mxl = 0, mnr = 1e9, mxr = 0;
      L(i, p, np) {
        int u = qwq[cnt][i];
        mnl = min(mnl, ql[u]);
        mxl = max(mxl, ql[u]);
        mxr = max(mxr, qr[u]);
        mnr = min(mnr, qr[u]);
      }
      L(i, mnr, mxr) { tlen[i] = 0; }
      L(i, mnl, mxl) { Len[i] = 1e9; }
      L(i, p, np) {
        int u = qwq[cnt][i];
        Len[ql[u]] = min(Len[ql[u]], qr[u]);
        tlen[qr[u]] = max(tlen[qr[u]], ql[u]);
      }
      ll sum = 0;
      L(l2, mnl, mxl) {
        ans += (ll)sum * (mxr - Len[l2] + 1);
        if (mnr <= l2 && l2 <= mxr) {
          sum += tlen[l2] - mnl + 1;
        }
      }
      p = np;
    }
  }
  cout << ans << '\n';
  return 0;
}