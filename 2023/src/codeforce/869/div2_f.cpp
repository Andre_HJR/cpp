#include <bits/stdc++.h>
using namespace std;

using PI = pair<int, int>;
using LL = long long;

const int N = 105;

int a[N];
string s[N];
bool vis[N];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  int n, k;
  cin >> n >> k;
  n -= 2;
  int m = (n + 1) / 2;
  if (k == m) {
    cout << "DL" << endl;
    return 0;
  }
  if (k < m) {
    for (int i = 0; i < m - k - 1; i++) {
      cout << "RDRU";
    }
    cout << "RDL" << endl;
    return 0;
  }
  for (int i = 0; i < k - m - 1; i++) {
    cout << "LDLU";
  }
  cout << "LDR";
  for (int i = 0; i < n; i++) {
    cout << "RDRU";
  }

  cout << "LU";

  for (int i = 0; i < m - 1; i++) {
    cout << "LDLU";
  }
  cout << "RU";
  cout << "RDL" << endl;
}
