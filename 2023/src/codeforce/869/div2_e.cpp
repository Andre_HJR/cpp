#include <bits/stdc++.h>
using namespace std;

using i64 = long long;
using vll = vector<long long>;
#define rep(i, a, b) for (int i = a; i < b; i++)
#define per(i, a, b) for (int i = a; i >= b; i--)
#define all(x) x.begin(), x.end()
#define rall(x) x.rbegin(), x.rend()
#define ins insert
#define fi first
#define se second
#define pb push_back
#define elif else if
const int o = 2.5e6 + 10;
const int MOD = 1e9 + 7;
inline int qp(int b, int f) {
  int res = 1;
  for (; f; f >>= 1, b = b * 1LL * b % MOD)
    if (f & 1) res = res * 1LL * b % MOD;
  return res;
}
int n, inv[o], a[o], b[o], u, v, w;
inline void calc(int& v, int* a) {
  rep(i, 0, n + 1) {
    v = (v + a[i] * (MOD - (n * (n + 1LL) / 2 - i) % MOD) % MOD * inv[i] % MOD *
                 inv[n - i] % MOD * (((n - i) & 1) ? MOD - 1 : 1)) %
        MOD;
  }
}

void milana() {
  cin >> n;
  inv[1] = 1;
  rep(i, 2, n + 1) { inv[i] = MOD - MOD / i * 1LL * inv[MOD % i] % MOD; }
  for (int i = inv[0] = 1; i <= n; i++) {
    inv[i] = inv[i - 1] * 1LL * inv[i] % MOD;
  }
  rep(i, 0, n + 1) { cin >> a[i]; }
  rep(i, 0, n + 1) { cin >> b[i]; }
  rep(i, 0, n + 1) {
    u = (u + a[i] * 1LL * inv[i] % MOD * inv[n - i] % MOD *
                 (((n - i) & 1) ? MOD - 1 : 1)) %
        MOD;
  }
  calc(v, a);
  calc(w, b);
  cout << (w + MOD - v) * 1LL * qp(u * 1LL * n % MOD, MOD - 2) % MOD;
}

int main() {
  ios::sync_with_stdio(false);
  cin.tie(nullptr);
  cout.tie(nullptr);

  int TEST = 1;
  // cin >> TEST;
  while (TEST-- > 0) {
    milana();
  }

  return 0;
}
