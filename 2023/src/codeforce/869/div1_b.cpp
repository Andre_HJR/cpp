#include <algorithm>
#include <iostream>
#include <istream>
#include <vector>

const int N = 2005;
std::vector<int> G[N];

int fa[N], vis[N];
int n, m;
bool is_nbr[N], used[N];

int root = 0;
int z = -1;

bool dfs(int v, int p) {
  vis[v] = 1;
  fa[v] = p;
  if (is_nbr[v] && p != root) {
    z = v;
    return 1;
  }
  for (int u : G[v]) {
    if (!vis[u]) {
      if (dfs(u, v)) return 1;
    }
  }
  return 0;
}

void solve() {
  std::cin >> n >> m;
  for (int i = 0; i < m; i++) {
    int u, v;
    std::cin >> u >> v;
    G[u].push_back(v);
    G[v].push_back(u);
  }

  for (int v = 1; v <= n; v++) {
    if ((int)G[v].size() < 4) continue;
    std::fill(vis + 1, vis + n + 1, 0);
    std::fill(is_nbr + 1, is_nbr + n + 1, 0);
    std::fill(used + 1, used + n + 1, 0);
    z = -1;
    root = v;
    for (int u : G[v]) is_nbr[u] = 1;
    if (dfs(v, v)) {
      std::cout << "YES\n";
      std::vector<std::pair<int, int>> edges;
      edges.emplace_back(z, v);
      used[z] = 1;
      int t = z;
      for (t = z; t != v; t = fa[t]) {
        edges.emplace_back(t, fa[t]);
        used[t] = 1;
        if (fa[t] == v) break;
      }
      int cnt = 0;
      for (int c : G[v]) {
        if (used[c]) continue;
        edges.emplace_back(c, v);
        cnt++;
        if (cnt == 2) break;
      }
      std::cout << edges.size() << '\n';
      for (auto& edge : edges)
        std::cout << edge.first << ' ' << edge.second << '\n';
      return;
    }
  }
  std::cout << "NO\n";
}

int main() {
  std::ios_base::sync_with_stdio(0);
  std::cin.tie(0);
  int T;
  std::cin >> T;
  while (T--) {
    solve();
    for (int i = 1; i <= n; i++) G[i].clear();
  }
}
