#include <iostream>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios_base::sync_with_stdio(0);
  std::cin.tie(0);
  int n, q;
  std::cin >> n >> q;
  std::vector<int> a(n), b(n);
  for (int& x : a) std::cin >> x;
  for (int i = 2; i < n; i++) {
    b[i] = b[i - 1] + (a[i] <= a[i - 1] && a[i - 1] <= a[i - 2]);
  }

  while (q--) {
    int l, r;
    std::cin >> l >> r;
    std::cout << r - l + 1 - (r == l ? 0 : b[r - 1] - b[l]) << '\n';
  }
  return 0;
}