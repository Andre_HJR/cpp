#include <iostream>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int x = 0, k = 0;
    std::cin >> x >> k;
    if (0 != x % k)
      std::cout << 1 << '\n' << x << '\n';
    else if (1 != x % k)
      std::cout << 2 << '\n' << 1 << ' ' << x - 1 << '\n';
    else
      std::cout << 2 << '\n' << -1 << ' ' << x + 1 << '\n';
  } while (--t);
  return 0;
}