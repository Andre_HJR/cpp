#include <algorithm>
#include <iostream>
#include <string>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0, i = 0;
    std::cin >> n;
    std::string s;
    std::cin >> s;
    int len = 2, ans = 2;
    for (i = 1; i < n && 0 != (len = s[i] != s[i - 1] ? 2 : len + 1);
         ans = std::max(ans, len), ++i)
      ;
    std::cout << ans << '\n';
  } while (--t);

  return 0;
}