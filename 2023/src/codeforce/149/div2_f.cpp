#include <iostream>
#include <queue>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios_base::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t;
  std::cin >> t;
  do {
    int n, k;
    std::cin >> n >> k;
    std::vector<int> a(n);
    for (int j = 0; j < n; j++) {
      std::cin >> a[j];
    }
    long long tv = 1000000000000000, fv = 0;
    while (tv - fv > 1) {
      long long mid = (tv + fv) / 2;
      std::vector<int> L(n + 1, 0);
      std::priority_queue<int> pq1;
      long long sum1 = 0;
      for (int j = 0; j < n; j++) {
        pq1.push(a[j]);
        sum1 += a[j];
        if (sum1 > mid) {
          sum1 -= pq1.top();
          pq1.pop();
        }
        L[j + 1] = pq1.size();
      }
      std::vector<int> R(n + 1, 0);
      std::priority_queue<int> pq2;
      long long sum2 = 0;
      for (int j = n - 1; j >= 0; j--) {
        pq2.push(a[j]);
        sum2 += a[j];
        if (sum2 > mid) {
          sum2 -= pq2.top();
          pq2.pop();
        }
        R[j] = pq2.size();
      }
      bool ok = false;
      for (int j = 0; j <= n; j++) {
        if (L[j] + R[j] >= k) {
          ok = true;
        }
      }
      if (ok) {
        tv = mid;
      } else {
        fv = mid;
      }
    }
    std::cout << tv << '\n';
  } while (--t);
  return 0;
}