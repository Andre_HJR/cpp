#include <iostream>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0, i = 0;
    std::cin >> n;
    std::string s;
    std::cin >> s;
    std::vector<int> S(n + 1, 0);
    for (i = 0; i < n; ++i) {
      if (s[i] == '(') S[i + 1] = S[i] + 1;
      if (s[i] == ')') S[i + 1] = S[i] - 1;
    }
    if (S[n] != 0)
      std::cout << -1 << '\n';
    else {
      bool pos = false, neg = false;
      for (i = 0; i < n; ++i) {
        if (S[i] > 0) pos = true;
        if (S[i] < 0) neg = true;
      }
      if (!pos || !neg) {
        std::cout << 1 << '\n';
        for (i = 0; i < n; ++i) {
          std::cout << 1;
          if (i < n - 1) std::cout << ' ';
        }
        std::cout << '\n';
      } else {
        std::vector<int> c(n);
        for (i = 0; i < n; ++i) {
          if (S[i] + S[i + 1] > 0)
            c[i] = 1;
          else
            c[i] = 2;
        }
        std::cout << 2 << '\n';
        for (i = 0; i < n; ++i) {
          std::cout << c[i];
          if (i < n - 1) std::cout << ' ';
        }
        std::cout << '\n';
      }
    }
  } while (--t);
  return 0;
}