#include <algorithm>
#include <iostream>
#include <iterator>
#include <vector>

// todo: Error in code
int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int i = 0;
    std::string s;
    std::cin >> s;
    int N = s.size();
    s = "0" + s + "1";
    for (i = 1; i < N + 2; i++) {
      if (s[i] == '?') {
        s[i] = s[i - 1];
      }
    }
    for (i = 1; i <= N; i++) {
      std::cout << s[i];
    }
    std::cout << '\n';
  } while (--t);
  return 0;
}