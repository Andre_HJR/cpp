#pragma GCC optimize("O3")
#pragma GCC optimize("unroll-loops")
#include <iostream>
#include <vector>
const long long MOD = 998244353;
int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int k;
  std::cin >> k;
  std::vector<int> a(1 << k);
  for (int i = 0; i < (1 << k); i++) {
    std::cin >> a[i];
    if (a[i] != -1) {
      a[i]--;
      if (a[i] != 0) {
        a[i] = 32 - __builtin_clz(a[i]);
      }
    }
  }
  std::vector<int> r(k + 1, 0);
  r[0] = 1;
  for (int i = 0; i < k; i++) {
    r[i + 1] = 1 << i;
  }
  for (int i = 0; i < (1 << k); i++) {
    if (a[i] != -1) {
      r[a[i]]--;
    }
  }
  std::vector<std::vector<long long>> dp(
      k + 1, std::vector<long long>(1 << (k + 1), 0));
  for (int i = 0; i < (1 << k); i++) {
    if (a[i] != -1) {
      dp[a[i]][(1 << k) + i] = 1;
    } else {
      for (int j = 0; j <= k; j++) {
        dp[j][(1 << k) + i] = 1;
      }
    }
  }
  for (int i = k; i >= 1; i--) {
    for (int j = 1 << (i - 1); j < (1 << i); j++) {
      for (int l = 0; l < i; l++) {
        dp[l][j] += dp[l][j * 2] * dp[i][j * 2 + 1];
        dp[l][j] += dp[i][j * 2] * dp[l][j * 2 + 1];
        dp[l][j] %= MOD;
      }
    }
  }
  long long ans = dp[0][1];
  for (int i = 0; i <= k; i++) {
    for (int j = 1; j <= r[i]; j++) {
      ans *= j;
      ans %= MOD;
    }
  }
  std::cout << ans << std::endl;
}