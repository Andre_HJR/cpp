#include <iostream>
#include <vector>

/*
You are given a permutation p of size n . You want to minimize the number of
subarrays of p that are permutations. In order to do so, you must perform the
following operation exactly once:

Select integers i , j , where 1<=i,j<=n , then
Swap pi and pj .

For example, if p=[5,1,4,2,3] and we choose i=2 , j=3 , the resulting array will
be [5,4,1,2,3] . If instead we choose i=j=5 , the resulting array will be
[5,1,4,2,3] .

Which choice of i and j will minimize the number of subarrays that are
permutations?

A permutation of length n is an array consisting of n distinct integers from 1
to n in arbitrary order. For example, [2,3,1,5,4] is a permutation, but [1,2,2]
is not a permutation (2 appears twice in the array), and [1,3,4] is also not a
permutation (n=3 but there is 4 in the array).

An array a is a subarray of an array b if a can be obtained from b by the
deletion of several (possibly, zero or all) elements from the beginning and
several (possibly, zero or all) elements from the end.

==================================================================================

Let idxx be the position of the element x in p , and consider what happens if
idxn is in between idx1 and idx2 . Notice that any subarray of size greater than
1 that is a permutation must contain idx1 and idx2 . So it must also contain
every index in between, including idxn . Therefore, n is an element of the
permutation subarray, so it must be of size at least n , and therefore must be
the whole array.

Therefore, if idxn is in between idx1 and idx2 , the only subarrays that are
permutations are [idx1,idx1] and [1,n] . These two subarrays will always be
permutations, so this is minimal.

To achieve this, we have 3 cases:

- If idxn lies in between idx1 and idx2 , swap idx1 and idx2 .
- If idxn<idx1,idx2 , swap idxn with the smaller of idx1 , idx2 .
- If idxn>idx1,idx2 , swap idxn with the larger of idx1 , idx2 .

In all three of these cases, after the swap, idxn will lie in between idx1 and
idx2 , minimizing the number of permutation subarrays.

Complexity: O(n)

*/
int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0;
    std::cin >> n;
    std::vector<int> a(n + 5, 0);
    for (int i = 1; i <= n; std::cin >> a[i], ++i)
      ;
    int id1 = 0, id2 = 0, id3 = 0;
    for (int i = 1; i <= n; ++i) {
      if (1 == a[i]) id1 = i;
      if (2 == a[i]) id2 = i;
      if (n == a[i]) id3 = i;
    }
    std::cout << id3 << ' '
              << id1 + id2 + id3 - std::min(id1, std::min(id2, id3)) -
                     std::max(id1, std::max(id2, id3))
              << '\n';
    // std::cout << id3 << ' '
    //           << std::sum(id1, id2, id3) - std::min(id1, id2, id3) -
    //                  std::max(id1, id2, id3)
  } while (--t);
  return 0;
}