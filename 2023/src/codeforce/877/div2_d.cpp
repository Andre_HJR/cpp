#include <iostream>
#include <set>
#include <vector>

namespace std::hjr::codeforce {
template <typename T>
struct FenwickTree {
  std::vector<T> sum;
  int lim;
  FenwickTree(int lim) {
    this->lim = lim;
    std::vector<T>(lim + 1, 0).swap(sum);
  }
  int lowbit(int x) { return x & (-x); }
  void init() { std::fill(sum.begin(), sum.end(), 0); }
  void add(int x, T val) {
    while (x <= lim) {
      sum[x] += val;
      x += lowbit(x);
    }
  }
  T get(int x) {
    T ret = 0;
    while (x) {
      ret += sum[x];
      x -= lowbit(x);
    }
    return ret;
  }
  T range(int l, int r) { return get(r) - get(l - 1); }
  T operator[](int idx) { return range(idx, idx); }
};
}  // namespace std::hjr::codeforce

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0, n = 0;
  std::set<int> bl;
  std::cin >> n >> t;
  for (int i = 0; i < n; ++i) {
    char c;
    std::cin >> c;
    bool b = c == '(';
    if (b ^ (i % 2 == 0)) {
      bl.insert(i);
    }
  }
  do {
    int i = 0;
    std::cin >> i;
    --i;
    if (n % 2 == 1) {
      std::cout << "NO\n";
      continue;
    }
    if (bl.count(i)) {
      bl.erase(i);
    } else {
      bl.insert(i);
    }
    std::cout << ((bl.empty() ||
                   (*bl.begin() % 2 == 1 && *bl.rbegin() % 2 == 0))
                      ? "YES"
                      : "NO")
              << '\n';
  } while (--t);
  return 0;
}