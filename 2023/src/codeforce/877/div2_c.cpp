#include <iostream>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int N = 0, M = 0;
    std::cin >> N >> M;
    std::vector<int> row(N, 0);
    if (0 == N % 2) {
      for (int i = 0; i < N / 2; ++i) {
        row[i] = i * 2 + 1, row[i + N / 2] = i * 2;
      }
    } else {
      // for (int i = 0; i < (N + 1) / 2; i++) row[i] = i * 2;
      // for (int i = 0; i < (N - 1) / 2; i++) row[i + (N + 1) / 2] = i * 2 + 1;
      for (int i = 0; i < N; i++) {
        row[i] = (i < (N + 1) / 2) ? i * 2 : (i - (N + 1) / 2) * 2 + 1;
      }
    }
    for (int i = 0; i < N; ++i) {
      for (int j = 1; j <= M; ++j) std::cout << row[i] * M + j << ' ';
      std::cout << '\n';
    }
    std::cout << '\n';
  } while (--t);
  return 0;
}