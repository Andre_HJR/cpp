#include <iostream>
#include <limits>
#include <vector>

/*
[puzzled!]

Two integers were written on a blackboard. After that, the following step was
carried out n−2 times:

Select any two integers on the board, and write the absolute value of their
difference on the board. After this process was complete, the list of n integers
was shuffled. You are given the final list. Recover one of the initial two
numbers. You do not need to recover the other one.

You are guaranteed that the input can be generated using the above process.
================================================================================================

[puzzled!]

Note that any negative integers on the board must have been one of the original
two numbers, because the absolute difference between any two numbers is
nonnegative.

So, if there are any negative numbers, print one of those.

If there are only nonnegative integers, note that the maximum number remains the
same after performing an operation, because for nonnegative integers a , b ,
where a <= b , we have |a−b|= b−a <= b Therefore, in this case, the maximum
number in the list must be one of the original numbers.

Complexity: O(n)
*/

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  std::vector<int> a(std::numeric_limits<short>::max(), 0);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0;
    a.clear();
    std::cin >> n;
    for (int i = 1; i <= n; std::cin >> a[i], ++i)
      ;
    int mn = std::numeric_limits<int>::max(),
        mx = std::numeric_limits<int>::min();
    for (int i = 1; i <= n;
         mn = std::min(mn, a[i]), mx = std::max(mx, a[i]), ++i)
      ;
    std::cout << (mn < 0 ? mn : mx) << '\n';
  } while (--t);
  return 0;
}