#include <iostream>

constexpr long long MOD = 1e9 + 7;

long long bp(long long a, int b) {
  long long c = 1;
  for (; b > 0; b /= 2) {
    if (b & 1) {
      c *= a;
      c %= MOD;
    }
    a *= a;
    a %= MOD;
  }
  return c;
}

long long inv(long long a) { return bp(a, MOD - 2); }

long long mod(long long a) { return (a % MOD + MOD) % MOD; }

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  do {
    int n = 0, m = 0, k = 0;
    std::cin >> n >> m >> k;
    for (int i = 0; i < n; ++i) {
      int a = 0;
      std::cin >> a;
    }
    long long ans = bp(k, m);
    long long c = 1, p = bp(k - 1, m);
    for (int i = 0; i < n; ++i) {
      ans = mod(ans - c * p);
      c = c * (m - i) % MOD * inv(i + 1) % MOD;
      p *= inv(k - 1);
      p %= MOD;
    }
    std::cout << ans << "\n";
  } while (--t);
  return 0;
}