#include <algorithm>
#include <iostream>
#include <map>

using T = long long;

int main(const int argc, const char** argv) {
  const T N = 2e5;
  const T MOD = 1e9 + 7;

  T t, n, m;
  T a[N + 5], F[N + 5], iF[N + 5], b[N + 5], z[N + 5];

  auto bp = [&](T x, T y) -> T {
    T res = 1;
    while (y > 0) {
      if (y & 1) res = (res * x) % MOD;
      x = (x * x) % MOD;
      y /= 2;
    }
    return res;
  };
  auto txl = [&] {
    F[0] = 1;
    for (T i = 1; i <= N; ++i) F[i] = (F[i - 1] * i) % MOD;
    iF[N] = bp(F[N], MOD - 2);
    for (T i = N; i >= 1; --i) iF[i - 1] = (iF[i] * i) % MOD;
  };

  auto C = [&](T k, T n) -> T {
    if (k > n) return 0LL;
    return (F[n] % MOD * iF[k] % MOD * iF[n - k] % MOD + MOD) % MOD;
  };
  std::ios::sync_with_stdio(0);
  std::cin.tie(0);
  txl();
  std::cin >> t;
  while (t--) {
    std::cin >> n >> m;
    std::map<T, T> cnt;
    for (T i = 1; i <= n; ++i) {
      std::cin >> a[i];
      ++cnt[a[i]];
      b[i] = 0LL;
      z[i] = 0LL;
    }
    b[0] = 1LL;
    z[0] = 0;
    std::sort(a + 1, a + n + 1);
    T tmp = 1LL;
    for (T i = 1; i <= n; ++i) {
      if (a[i] != a[i - 1])
        b[i] = (b[i - 1] * cnt[a[i]]) % MOD;
      else
        b[i] = b[i - 1];
      z[i] = z[i - 1];
      if (a[i] != a[i - 1]) ++z[i];
    }
    T ans = 0LL;
    for (T i = 1; i <= n; ++i) {
      if (a[i] == a[i - 1]) continue;
      T k = std::lower_bound(a + i, a + n + 1, a[i] + m) - a;
      --k;
      T l = k - i + 1;
      T h = z[k] - z[i - 1];
      if (!ans)
        ans = (C(m, h) * (b[k] * bp(b[i - 1], MOD - 2))) % MOD;
      else
        ans = (ans + (C(m, h) * (b[k] * bp(b[i - 1], MOD - 2))) % MOD) % MOD;
    }
    std::cout << ans << "\n";
  }
}