#include <algorithm>
#include <iostream>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  int minn = 2e9;
  std::cin >> t;
  while (t--) {
    int n = 0;
    std::cin >> n;
    std::vector<int> a(n);
    for (int& i : a) std::cin >> i;
    std::sort(a.begin(), a.end());
    a.resize(std::unique(a.begin(), a.end()) - a.begin());
    int need = a[0] % 2;
    int have[2] = {};
    have[a[0] % 2] = 1;
    bool good = true;
    for (int i = 1; i < a.size(); ++i) {
      int cv = (a[i] % 2) ^ need;
      if (!have[cv] && need != (a[i] % 2)) {
        good = false;
        break;
      }
      have[a[i] % 2] = 1;
    }

    std::cout << (good ? "YES" : "NO") << '\n';
  }
  return 0;
}