#include <algorithm>
#include <iostream>
#include <vector>

int main(const int argc, const char **argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  while (t--) {
    int n = 0, k = 0, x = 0;
    std::cin >> n >> k;
    std::vector<std::pair<int, int>> a, b;
    for (int i = 0; i < n; ++i) x = 0, std::cin >> x, a.push_back({x, i});
    for (int i = 0; i < n; ++i) x = 0, std::cin >> x, b.push_back({x, i});
    std::sort(a.begin(), a.end());
    std::sort(b.begin(), b.end());
    std::vector<int> ans(n);
    for (int i = 0; i < n; ++i) ans[a[i].second] = b[i].first;
    std::for_each(ans.begin(), ans.end(),
                  [](const auto i) { std::cout << i << " "; });
    std::cout << '\n';
  }
  return 0;
}