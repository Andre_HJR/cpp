#include <algorithm>
#include <cstdlib>
#include <iostream>
#include <iterator>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  while (t--) {
    int n = 0, i = 0;
    std::cin >> n;
    std::vector<std::vector<int>> e(n + 1);
    std::vector<std::pair<int, int>> temp(n);
    std::vector<int> pa(n + 1);
    int a = 0, b = 0;
    for (i = 1; i <= n - 1; ++i) {
      a = b = 0;
      std::cin >> a >> b;
      temp[i] = {a, b};
      e[a].push_back(b);
      e[b].push_back(a);
    }
    if (n % 3) {
      std::cout << -1 << '\n';
      continue;
    }
    std::vector<int> sz(n + 1, 0);
    std::vector<int> ans;
    auto dfs = [&](auto& self, int u, int fa) -> void {
      sz[u] = 1;
      for (auto v : e[u])
        if (v != fa) {
          self(self, v, u);
          pa[v] = u;
          sz[u] += sz[v];
        }
    };
    dfs(dfs, 1, -1);
    for (i = 1; i < n; ++i) {
      std::pair<int, int> _t = temp[i];
      if (_t.first == pa[_t.second]) std::swap(_t.first, _t.second);
      if (0 == sz[_t.first] % 3) ans.push_back(i);
    }
    if (ans.size() == n / 3 - 1) {
      std::cout << ans.size() << "\n",
          // std::for_each(ans.begin(), ans.end(),
          //               [](int& i) { std::cout << i << ' '; }),
          std::copy(ans.begin(), ans.end(),
                    std::ostream_iterator<int>(std::cout, " ")),
          std::cout << '\n';
    } else
      std::cout << -1 << '\n';
  }
  return 0;
}