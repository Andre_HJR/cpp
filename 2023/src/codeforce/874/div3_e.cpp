#include <iostream>
#include <set>
#include <vector>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  while (t--) {
    int n = 0, i = 0;
    std::cin >> n;
    std::vector<int> a(n, 0);
    std::vector<std::set<int>> g(n, std::set<int>());
    for (i = 0; i < n; ++i) {
      std::cin >> a[i];
      --a[i];
      g[i].insert(a[i]);
      g[a[i]].insert(i);
    }
    int max = 0;
    std::vector<bool> used(n, false);
    int min = 0, cnt = 0, V = 0;
    auto dfs = [&](auto& self, int v, int p) -> void {
      ++V;
      used[v] = true;
      for (auto u : g[v]) {
        ++cnt;
        if (u == p) continue;
        if (used[u]) continue;
        self(self, u, v);
      }
    };
    bool ok = false;
    for (i = 0; i < n; ++i) {
      if (!used[i]) {
        cnt = 0;
        V = 0;
        dfs(dfs, i, i);
        ++max;
        cnt /= 2;
        if (V == cnt)
          ++min;
        else {
          if (!ok) ++min, ok = true;
        }
      }
    }
    std::cout << min << " " << max << "\n";
  }
  return 0;
}