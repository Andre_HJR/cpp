#include <algorithm>
#include <iostream>
#include <set>
#include <string>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int n = 0, m = 0, i = 0, j = 0, t = 0;
  std::set<std::string> _s;
  std::string _t;
  std::cin >> t;
  while (t--) {
    n = m = i = j = 0;
    _s.clear();
    _t.clear();
    std::cin >> n >> _t;
    for (i = 1; i < n; ++i) _s.insert(_t.substr(i - 1, 2));
    std::cout << _s.size() << '\n';
  }
  return 0;
}
