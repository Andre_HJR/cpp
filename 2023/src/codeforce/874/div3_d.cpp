#include <algorithm>
#include <iostream>
#include <string>
#include <vector>
int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  std::cin.tie(nullptr);
  int t = 0;
  std::cin >> t;
  while (t--) {
    int n = 0;
    std::cin >> n;
    std::vector<int> a(n, 0);
    std::for_each(a.begin(), a.end(), [](int& i) { std::cin >> i; });
    int l = 0, r = 0, i = 0;
    l = r = std::max_element(a.begin() + 1, a.end()) - a.begin() - 1;
    if (n - 2 == r) ++r, ++l;
    while (l > 1 && a[l - 1] > a[0]) --l;
    std::reverse(a.begin() + l, a.begin() + r + 1);
    for (i = r + 1; i < n; ++i) std::cout << a[i] << " ";
    for (i = l; i <= r; ++i) std::cout << a[i] << " ";
    for (i = 0; i < l; ++i) std::cout << a[i] << " ";
    std::cout << "\n";
  }

  return 0;
}