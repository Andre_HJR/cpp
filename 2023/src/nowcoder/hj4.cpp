#include <iostream>

int main(const int argc, const char** argv) {
  std::string s;
  while (std::cin >> s) {
    while (s.size() > 8) {
      std::cout << s.substr(0, 8) << std::endl;
      s = s.substr(8);
    }
    s.resize(8, '0');
    std::cout << s << std::endl;
  }
}