#include <algorithm>
#include <iostream>
#include <map>
#include <vector>

int main() {
  int n = 0, m = 0;
  while (std::cin >> n) {
    std::vector<int> I(n);
    for (int i = 0; i < n; i++) std::cin >> I[i];
    std::cin >> m;
    std::map<int, int> R;
    for (int i = 0; i < m; i++) {
      int t;
      std::cin >> t;
      R[t] = 1;
    }
    int total = 0;
    std::string res = "";
    for (auto iter = R.begin(); iter != R.end(); iter++) {
      int count = 0;
      std::string temp = "";
      for (int i = 0; i < n; i++) {
        if (std::to_string(I[i]).find(std::to_string(iter->first)) !=
            std::string::npos) {
          count++;
          temp += std::to_string(i) + ' ' + std::to_string(I[i]) + ' ';
        }
      }
      if (count != 0) {
        res += std::to_string(iter->first) + ' ' + std::to_string(count) + ' ' +
               temp;
        total += (2 * count + 2);
      }
    }
    res = std::to_string(total) + ' ' + res;
    std::cout << res << std::endl;
  }
  return 0;
}
