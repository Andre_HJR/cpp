#include <algorithm>
#include <iostream>
#include <vector>

int main(const int argc, const char** argv) {
  int n = 0, tmp = 0, i = 0, j = 0;
  std::vector<int> heights;
  while (std::cin >> n) {
    for (i = 0; i < n; i++) {
      std::cin >> tmp;
      heights.push_back(tmp);
    }
    std::vector<int> dp_h(n), dp_t(n);
    std::vector<int> cache;

    for (i = 0; i < n; i++) {
      int potIndex = std::lower_bound(cache.begin(), cache.end(), heights[i]) -
                     cache.begin();
      dp_h[i] = potIndex + 1;
      if (potIndex < cache.size())
        cache[potIndex] = heights[i];
      else
        cache.push_back(heights[i]);
    }
    cache.clear();
    for (i = n - 1; i >= 0; i--) {
      int potIndex = std::lower_bound(cache.begin(), cache.end(), heights[i]) -
                     cache.begin();
      dp_t[i] = potIndex + 1;
      if (potIndex < cache.size())
        cache[potIndex] = heights[i];
      else
        cache.push_back(heights[i]);
    }
    int maxNum = 0;
    for (i = 0; i < n; i++) {
      if (dp_t[i] + dp_h[i] - 1 > maxNum) maxNum = dp_t[i] + dp_h[i] - 1;
    }
    std::cout << n - maxNum << std::endl;
    heights.clear();
  }
  return 0;
}
