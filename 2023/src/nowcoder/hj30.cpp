#include <algorithm>
#include <iostream>
using namespace std;
void Process_String(string str1, string str2, string strOutput) {
  char Intput[] = {"0123456789abcdefABCDEF"};
  char Output[] = {"084C2A6E195D3B7F5D3B7F"};
  strOutput = str1 + str2;
  string odd_str;
  string even_str;
  for (int i = 0; i < strOutput.size(); i++) {
    if (i % 2 == 0) {
      odd_str += strOutput[i];
    } else if (i % 2 == 1) {
      even_str += strOutput[i];
    }
  }
  sort(odd_str.begin(), odd_str.end());
  sort(even_str.begin(), even_str.end());
  int j = 0;
  int k = 0;
  for (int i = 0; i < strOutput.size(); i++) {
    if (i % 2 == 0) {
      strOutput[i] = odd_str[j];
      j++;
    } else if (i % 2 == 1) {
      strOutput[i] = even_str[k];
      k++;
    }
  }
  for (int i = 0; i < strOutput.size(); i++) {
    if ((strOutput[i] >= '0') && (strOutput[i] <= '9')) {
      strOutput[i] = Output[strOutput[i] - '0'];
    } else if ((strOutput[i] >= 'a') && (strOutput[i] <= 'f')) {
      strOutput[i] = Output[strOutput[i] - 'a' + 10];
    } else if ((strOutput[i] >= 'A') && (strOutput[i] <= 'F')) {
      strOutput[i] = Output[strOutput[i] - 'A' + 16];
    }
  }
  cout << strOutput << endl;
  return;
}
int main(const int argc, const char** argv) {
  string str1, str2, strOutput;
  while (cin >> str1 >> str2) {
    Process_String(str1, str2, strOutput);
  }
  return 0;
}
