#include <algorithm>
#include <iostream>
#include <string>

int main(const int argc, const char** argv) {
  std::string s;
  std::getline(std::cin, s);
  std::copy(s.rbegin(), s.rend(), std::ostreambuf_iterator<char>(std::cout));
  return 0;
}