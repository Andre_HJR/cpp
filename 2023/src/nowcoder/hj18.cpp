#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main() {
  vector<int> arr(7, 0);
  string s;
  while (getline(cin, s)) {
    int n = s.length();
    vector<int> ips;
    bool bad = false;
    bool isnum = false;
    int num = 0;
    for (int i = 0; i < n; i++) {
      if (s[i] == '.' || s[i] == '~') {
        if (isnum) {
          if (num > 255) {
            bad = true;
            break;
          }
          ips.push_back(num);
          isnum = false;
          num = 0;
        } else {
          arr[5]++;
          bad = true;
          break;
        }
      } else if (s[i] >= '0' && s[i] <= '9') {
        isnum = true;
        num = num * 10 + s[i] - '0';
      } else {
        arr[5]++;
        isnum = false;
        bad = true;
        break;
      }
    }
    if (isnum) ips.push_back(num);
    if (ips[0] == 0 || ips[0] == 127 || bad) continue;
    int mask = 4;
    while (mask < 8 && ips[mask] == 255) mask++;
    if (mask == 8) {
      arr[5]++;
      continue;
    } else if (ips[mask] == 254 || ips[mask] == 252 || ips[mask] == 248 ||
               ips[mask] == 240 || ips[mask] == 224 || ips[mask] == 191 ||
               ips[mask] == 128)
      mask++;
    while (mask < 8 && ips[mask] == 0) mask++;
    if (mask != 8) {
      arr[5]++;
      continue;
    }
    if (ips[0] >= 1 && ips[0] <= 126)
      arr[0]++;
    else if (ips[0] >= 128 && ips[0] <= 191)
      arr[1]++;
    else if (ips[0] >= 192 && ips[0] <= 223)
      arr[2]++;
    else if (ips[0] >= 224 && ips[0] <= 239)
      arr[3]++;
    else if (ips[0] >= 240 && ips[0] <= 255)
      arr[4]++;
    if (ips[0] == 10)
      arr[6]++;
    else if (ips[0] == 172 && (ips[1] >= 16 && ips[1] <= 31))
      arr[6]++;
    else if (ips[0] == 192 && ips[1] == 168)
      arr[6]++;
  }
  for (int i = 0; i < 7; i++) {
    cout << arr[i];
    if (i != 6) cout << " ";
  }
  cout << endl;
  return 0;
}
