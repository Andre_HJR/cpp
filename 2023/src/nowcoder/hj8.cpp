#include <iostream>
#include <map>

int main(const int argc, const char** argv) {
  int n = 0;
  std::map<int, int> mp;
  std::pair<int, int> tmp;
  std::cin >> n;
  do {
    tmp.first = 0;
    tmp.second = 0;
    std::cin >> tmp.first >> tmp.second;
    mp[tmp.first] += tmp.second;
  } while (--n);  //     for (; n; --n) {}
  for (auto tmp : mp) std::cout << tmp.first << " " << tmp.second << std::endl;
  return 0;
}