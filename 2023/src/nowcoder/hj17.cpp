#include <iostream>
#include <map>
#include <regex>
#include <string>

using namespace std;

int main(int argc, char *argv[]) {
  string str, temp, pattern = "[ADWS]\\d{1,2}$";
  regex r(pattern);

  pair<int, int> p = {0, 0};
  while (getline(cin, str, ';')) {
    if (!regex_match(str, r)) continue;
    switch (str[0]) {
      case 'A': {
        p.first -= stoi(string(str.begin() + 1, str.end()));
        break;
      }
      case 'D': {
        p.first += stoi(string(str.begin() + 1, str.end()));
        break;
      }
      case 'W': {
        p.second += stoi(string(str.begin() + 1, str.end()));
        break;
      }
      case 'S': {
        p.second -= stoi(string(str.begin() + 1, str.end()));
        break;
      }
    }
  }
  cout << p.first << "," << p.second << endl;
  return 0;
}