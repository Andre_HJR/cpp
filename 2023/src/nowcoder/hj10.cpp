#include <iostream>
#include <set>
#include <string>

int main(const int argc, const char** argv) {
  std::string s;
  std::getline(std::cin, s);
  std::set<char> st;
  for (auto i : s) st.insert(i);
  std::cout << st.size() << std::endl;
  return 0;
}