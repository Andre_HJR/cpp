#include <array>
#include <iostream>
#include <vector>

enum { N = 70, M = 32010 };
int n = 0, m = 0;
std::array<int, M> f;
std::array<std::pair<int, int>, N> masters;
std::array<std::vector<std::pair<int, int>>, N> accessory;

int main(const int argc, const char** argv) {
  std::cin >> m >> n;
  for (int i = 1; i <= n; i++) {
    int v, p, q;
    std::cin >> v >> p >> q;
    if (q == 0)
      masters[i] = {v, v * p};
    else {
      accessory[q].push_back({v, v * p});
    }
  }
  for (int i = 1; i <= n; i++) {
    for (int u = m; u >= 0; u--) {
      for (int j = 0; j < 1 << accessory[i].size(); j++) {
        int volume = masters[i].first;
        int value = masters[i].second;
        for (int k = 0; k < accessory[i].size(); k++) {
          if (j >> k & 1) {
            volume += accessory[i][k].first;
            value += accessory[i][k].second;
          }
        }
        if (u >= volume) f[u] = std::max(f[u], f[u - volume] + value);
      }
    }
  }
  std::cout << f[m] << std::endl;
  return 0;
}