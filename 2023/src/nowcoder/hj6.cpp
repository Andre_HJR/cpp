#include <iostream>

int main(const int argc, const char** argv) {
  int num = 0;
  std::cin >> num;
  for (int i = 2; i * i <= num; ++i) {
    for (; 0 == num % i; num /= i) std::cout << i << " ";
  }
  if (num > 1) std::cout << num << " ";
  return 0;
}