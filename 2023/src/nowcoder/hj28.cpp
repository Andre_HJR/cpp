#include <iostream>
#include <vector>

bool isprime(int num) {
  for (int i = 2; i * i <= num; i++) {
    if (num % i == 0) return false;
  }
  return true;
}

bool find(int index, std::vector<bool>& used, std::vector<int>& match,
          std::vector<std::vector<bool>>& map) {
  for (int i = 0; i < used.size(); i++) {
    if (map[i][index] && !used[i]) {
      used[i] = true;
      if (match[i] == -1 || find(match[i], used, match, map)) {
        match[i] = index;
        return true;
      }
    }
  }
  return false;
}
int main(const int argc, const char** argv) {
  int n;
  while (std::cin >> n) {
    std::vector<int> odds;
    std::vector<int> evens;
    std::vector<int> nums(n);
    for (int i = 0; i < n; i++) {
      std::cin >> nums[i];
      if (nums[i] % 2)
        odds.push_back(nums[i]);
      else
        evens.push_back(nums[i]);
    }
    int count = 0;
    if (odds.size() == 0 || evens.size() == 0) {
      std::cout << count << std::endl;
      continue;
    }
    std::vector<std::vector<bool>> map(evens.size(),
                                       std::vector<bool>(odds.size(), false));
    for (int i = 0; i < evens.size(); i++) {
      for (int j = 0; j < odds.size(); j++) {
        if (isprime(evens[i] + odds[j])) map[i][j] = true;
      }
    }
    std::vector<int> match(evens.size(), -1);
    for (int i = 0; i < odds.size(); i++) {
      std::vector<bool> used(evens.size(), false);
      if (find(i, used, match, map)) count++;
    }
    std::cout << count << std::endl;
  }
  return 0;
}
