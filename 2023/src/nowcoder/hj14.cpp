#include <algorithm>
#include <iostream>
#include <vector>

int main(const int argc, const char** argv) {
  int n = 0;
  std::cin >> n;
  std::string s;
  std::vector<std::string> vs;
  while (std::cin >> s) vs.push_back(s);
  std::sort(vs.begin(), vs.end());
  std::for_each(vs.begin(), vs.end(),
                [](std::string& i) { std::cout << i << std::endl; });
  return 0;
}