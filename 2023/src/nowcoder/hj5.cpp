#include <iostream>
#include <string>

int main(const int argc, const char** argv) {
  std::string s;
  std::size_t* idx = nullptr;
  int base = 16;
  while (std::cin >> s) {
    std::cout << std::stoi(s, idx, base) << std::endl;
  }
  return 0;
}