#include <iostream>
#include <set>
int main(const int argc, const char** argv) {
  std::set<int> st;
  int n = 0, x = 0;
  std::cin >> n;
  for (int i = 0; i < n; i++) {
    std::cin >> x;
    st.insert(x);
  }
  for (auto i : st) std::cout << i << std::endl;
  return 0;
}
