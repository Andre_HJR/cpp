#include <array>
#include <iostream>

int main(const int argc, const char** argv) {
  enum { ALAPHA = 26 };
  std::string s;
  std::cin >> s;
  std::array<int, ALAPHA> num{0};
  int min = 20;
  for (auto i : s) ++num[i - 'a'];
  for (int i = 0; i < ALAPHA; ++i)
    if (num[i] != 0 && min > num[i]) min = num[i];
  for (auto i : s)
    if (num[i - 'a'] > min) std::cout << i;
  return 0;
}