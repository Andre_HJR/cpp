#include <iostream>

int main(const int argc, const char** argv) {
  double a;
  std::cin >> a;
  int b = static_cast<int>(a);
  double c = a - b;
  if (c >= 0.5) ++b;
  std::cout << b;
  return 0;
}