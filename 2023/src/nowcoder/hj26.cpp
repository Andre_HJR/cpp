#include <iostream>
#include <vector>

enum { ALAPHA = 26 };

std::string String_Sorting(std::string str) {
  int len = str.size();
  std::vector<char> vec;
  for (int j = 0; j < ALAPHA; j++) {
    for (int i = 0; i < len; i++) {
      if ((str[i] - 'a' == j) || (str[i] - 'A' == j)) {
        vec.push_back(str[i]);
      }
    }
  }
  for (int i = 0, k = 0; (i < len) && (k < vec.size()); i++) {
    if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z')) {
      str[i] = vec[k++];
    }
  }
  return str;
}
int main(const int argc, const char** argv) {
  std::string str;
  while (std::getline(std::cin, str)) {
    std::cout << String_Sorting(str) << std::endl;
  }
  return 0;
}
