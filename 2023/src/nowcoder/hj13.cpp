#include <iostream>
#include <iterator>
#include <string>
#include <vector>

int main(const int argc, const char** argv) {
  std::vector<std::string> vs;
  std::string ts;
  while (std::cin >> ts) vs.emplace_back(ts);
  std::copy(vs.rbegin(), vs.rend(),
            std::ostream_iterator<std::string>(std::cout, " "));
  return 0;
}