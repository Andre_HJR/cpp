#include <iostream>
#include <regex>
#include <string>
using namespace std;

int main() {
  string s;
  while (cin >> s) {
    if (s.length() <= 8) {
      cout << "NG" << endl;
      continue;
    }
    string re[4] = {"[a-z]", "[A-Z]", "\\d", "[^a-zA-Z0-9]"};
    int count = 0;
    for (int i = 0; i < 4; i++) {
      regex pattern(re[i]);
      if (regex_search(s, pattern)) count++;
    }
    if (count < 3) {
      cout << "NG" << endl;
      continue;
    }
    regex pattern(".*(...)(.*\\1).*");
    if (regex_search(s, pattern))
      cout << "NG" << endl;
    else
      cout << "OK" << endl;
  }
  return 0;
}
