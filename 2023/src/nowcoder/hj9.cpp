#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_set>

int main(const int argc, const char** argv) {
  std::string s;
  std::cin >> s;
  std::unordered_set<char> st;
  std::reverse(s.begin(), s.end());
  for (auto i : s) {
    if (st.count(i)) continue;
    st.insert(i);
    std::cout << i;
  }
  return 0;
}