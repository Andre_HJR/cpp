#include <algorithm>
#include <iostream>
#include <string>

int main(const int argc, const char** argv) {
  std::string s;
  std::getline(std::cin, s);
  char c = std::tolower(getchar());
  std::cout << std::count_if(s.begin(), s.end(), [&c](char i) {
    return c == std::towlower(i);
  }) << std::endl;
  return 0;
}