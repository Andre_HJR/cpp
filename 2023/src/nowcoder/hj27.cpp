#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

bool isbrother(std::string s1, std::string s2) {
  if (s1.length() == s2.length()) {
    if (s1 == s2) return false;
    std::unordered_map<char, int> mp1;
    std::unordered_map<char, int> mp2;
    for (int i = 0; i < s1.length(); i++) mp1[s1[i]]++;
    for (int i = 0; i < s2.length(); i++) mp2[s2[i]]++;
    auto it = mp1.begin();
    while (it != mp1.end()) {
      if (mp2.find(it->first) == mp2.end() || mp2[it->first] != it->second) {
        return false;
      }
      it++;
    }
    return true;
  }
  return false;
}
int main(const int argc, const char** argv) {
  int n;
  while (std::cin >> n) {
    std::vector<std::string> strs(n);
    for (int i = 0; i < n; i++) std::cin >> strs[i];
    std::string str;
    std::cin >> str;
    int k;
    std::cin >> k;
    std::vector<std::string> brothers;
    for (int i = 0; i < n; i++) {
      if (isbrother(str, strs[i])) brothers.push_back(strs[i]);
    }
    std::sort(brothers.begin(), brothers.end());
    std::cout << brothers.size() << std::endl;
    if (brothers.size() >= k) std::cout << brothers[k - 1] << std::endl;
  }
  return 0;
}
