#include <bitset>
#include <iostream>

int main(const int argc, const char** argv) {
  int n;
  std::cin >> n;
  std::bitset<32> b(n);
  std::cout << b.count();
  return 0;
}