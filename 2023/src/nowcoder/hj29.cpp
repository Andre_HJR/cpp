#include <iostream>

int main() {
  std::string str1, str2;
  std::cin >> str1 >> str2;
  std::string a(
      "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
  std::string b(
      "BCDEFGHIJKLMNOPQRSTUVWXYZAbcdefghijklmnopqrstuvwxyza1234567890");
  for (char& i : str1) i = b[a.find_last_of(i)];
  for (char& i : str2) i = a[b.find_last_of(i)];
  std::cout << str1 << std::endl << str2;
}
