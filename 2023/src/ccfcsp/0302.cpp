/**
 * @file 0302.cpp
 * @author andre HJR (3191971830@qq.com)
 * @brief 2023-03-02: 垦田计划
 * @version 0.1
 * @date 2023-04-27
 *
 * @copyright Copyright (c) 2023
 *
 * @details
 *
 * About test Range
 * bool:        0x1
 * short:       0x7fff
 * int:         0x7fffffff (10^5 = 0xf4240,10^9 = 0x3B9ACA00)
 * streamsize:  0x7fffffffffffffff
 * size_t:      0xffffffffffffffff
 * char:        0x7f
 * char16_t:    0xffff
 * wchar_t:     0x7fffffff
 * float:       0x1.fffffep+127
 * double:      0x1.fffffffffffffp+1023
 * long double: 0xf.fffffffffffffffp+16380
 */

#include <array>
#include <iostream>
const int N = 1e5 + 10;
std::array<int, N> sa;  // 记录同一基础耗时缩减一天所需要的总资源

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  // int:         0x7fffffff (10^5 = 0xf4240,10^9 = 0x3B9ACA00)
  int n = 0,    // 待开垦的区域总数
      m = 0,    // 顿顿手上的资源数量
      k = 0,    // 每块区域的最少开垦天数
      t_i = 0,  // 第t_i块区域开垦耗时
      c_i = 0,  // 将耗时缩短1天所需资源数量
      mx = 0,   // mx记录基础耗时的最大值
      day = 0,  // day记录结果
      i = 0;    // for-loop , identity
  std::cin >> n >> m >> k;
  i = 1;
  for (; i <= n; mx = std::max(mx, t_i), i++) {
    t_i = c_i = 0, std::cin >> t_i >> c_i,
    sa[t_i] += c_i;  // 记录同一基础耗时缩减一天所需要的总资源
  }
  i = day = mx;  // 记录结果
  for (; i >= k /* i -> k, k is bound of bottom. */;
       day > k ? --day, --i : --i /* day 一定要尽可能的减少 */) {
    if (sa[day] > m) break;  // 超过了自己已有的资源数
    m -= sa[day],  // 同一基础耗时的同时减少一天，去除已经消耗的资源
        sa[day - 1] += sa[day];  // 下一天的需要加上上一天的资源总数
  }
  std::cout << day;
  return 0;
}