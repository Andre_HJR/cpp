#include <algorithm>
#include <array>
#include <bitset>
#include <cstring>
#include <iostream>
#include <map>
#include <vector>

const int N = 2502;
std::array<int, N> num;
std::array<int, N> to;    // [id]
std::map<int, int> q[N];  // 三维向量 (id, attr_name, attr_value)
std::map<std::pair<int, int>, std::vector<int>>
    p;                                // (attr_name, attr_value) -> Vec_ID {}
std::map<int, std::vector<int>> has;  // attr_name -> Vec_ID {}

std::array<char, N> op;  // 存储相应的操作 & |
int f[N];                // 存储需要输出的id
char s[N];               // 存储操作的表达式, 这是一个缓冲区

std::bitset<N> stk[N * 2];
std::bitset<N> cal(int l, char x, int r) {
  std::bitset<N> ans;
  for (auto &v : p[std::pair<int, int>(l, r)]) {
    ans.set(v);  // 将 std::bitset 中位于 pos 位置的值设为 1
  }
  if (x == '~') {
    for (auto &v : has[l]) {
      ans.flip(v);  // 将 std::bitset 中位于 v 位置的值取反
    }
  }
  return ans;
}

// 对 s 进行解析, 遵循如下的 BNF 的范式
// NON_ZERO_DIGIT =  "1" / "2" / "3" / "4" /
//                   "5" / "6" / "7" / "8" / "9"
// DIGIT          =  "0" / NON_ZERO_DIGIT
// NUMBER         =  NON_ZERO_DIGIT / (NON_ZERO_DIGIT DIGIT*)
// ATTRIBUTE      =  NUMBER
// VALUE          =  NUMBER
// OPERATOR       =  ":" / "~"
// BASE_EXPR      =  ATTRIBUTE OPERATOR VALUE
// LOGIC          =  "&" / "|"
// EXPR           =  BASE_EXPR / (LOGIC "(" EXPR ")" "(" EXPR ")")
//
// EASY_EXPR      =  BASE_EXPR /
//                   (LOGIC "(" BASE_EXPR ")" "(" BASE_EXPR ")")
#define expression_parse()                                                                                                                                                                                                                                                                                                     \
  for (int j = 0; j < sz;) {                                                                                                                                                                                                                                                                                                   \
    if (s[j] == '&' || s[j] == '|') {                                                                                                                                                                                                                                                                                          \
      /*操作符有两种：与（&）和或（|）。如果操作符为与，则当且仅当两个表达式都与某一用户相匹配时，该表达式与该用户相匹配；如果操作符为或，则当且仅当两个表达式中至少有一个与某一用户相匹配时，该表达式与该用户相匹配。*/ \
      op[++c] = s[j++];                                                                                                                                                                                                                                                                                                        \
    } else if (s[j] == '(') {                                                                                                                                                                                                                                                                                                  \
      j++;                                                                                                                                                                                                                                                                                                                     \
    } else if (s[j] == ')') {                                                                                                                                                                                                                                                                                                  \
      num[c]++;                                                                                                                                                                                                                                                                                                                \
      if (num[c] == 2) {                                                                                                                                                                                                                                                                                                       \
        d--;                                                                                                                                                                                                                                                                                                                   \
        if (op[c] == '&')                                                                                                                                                                                                                                                                                                      \
          stk[d] = stk[d] & stk[d + 1];                                                                                                                                                                                                                                                                                        \
        else                                                                                                                                                                                                                                                                                                                   \
          stk[d] = stk[d] | stk[d + 1];                                                                                                                                                                                                                                                                                        \
        num[c--] = 0;                                                                                                                                                                                                                                                                                                          \
      }                                                                                                                                                                                                                                                                                                                        \
      j++;                                                                                                                                                                                                                                                                                                                     \
    } else { /* 操作符有两种：断言与反断言。断言操作符为                                                                                                                                                                                                                                                   \
                :，表示匹配具有该属性且值与之相等的用户；反断言操作符为                                                                                                                                                                                                                             \
                ~，表示匹配具有该属性且值与之不等的用户。 */                                                                                                                                                                                                                                               \
      int cur = j, l = 0, r = 0;                                                                                                                                                                                                                                                                                               \
      while (cur < sz && (s[cur] != ':' && s[cur] != '~')) {                                                                                                                                                                                                                                                                   \
        l = l * 10 + (s[cur] - '0');                                                                                                                                                                                                                                                                                           \
        cur++;                                                                                                                                                                                                                                                                                                                 \
      }                                                                                                                                                                                                                                                                                                                        \
      char x = s[cur++];                                                                                                                                                                                                                                                                                                       \
      while (cur < sz && s[cur] != ')') {                                                                                                                                                                                                                                                                                      \
        r = r * 10 + (s[cur] - '0');                                                                                                                                                                                                                                                                                           \
        cur++;                                                                                                                                                                                                                                                                                                                 \
      }                                                                                                                                                                                                                                                                                                                        \
      stk[++d] = cal(l, x, r);                                                                                                                                                                                                                                                                                                 \
      j = cur;                                                                                                                                                                                                                                                                                                                 \
    }                                                                                                                                                                                                                                                                                                                          \
  }

#define usertable_read()                                                                                                                    \
  for (int i = 1; i <= n; ++i) {                                                                                                            \
    std::cin >> id >> k; /* 获取用户的id和其相应的属性个数*/                                                                  \
    to[i] = id;                                                                                                                             \
    for (int j = 1; j <= k; ++j) {                                                                                                          \
      std::cin >> x >>                                                                                                                      \
          y; /* 每两个正整数表示该用户具有的一个属性及其值。这些属性按照属性编号从小到大的顺序给出 \
              */                                                                                                                            \
      q[i][x] = y;                                                                                                                          \
      has[x].push_back(i);                                                                                                                  \
      p[std::pair<int, int>(x, y)].push_back(i);                                                                                            \
    }                                                                                                                                       \
  }

#define output_id()                                                          \
  if (!e) {                                                                  \
    std::cout << '\n';                                                       \
  } else {                                                                   \
    std::sort(f + 1, f + e + 1);                                             \
    for (int j = 1; j <= e; ++j) std::cout << f[j] << (j == e ? '\n' : ' '); \
  }
int main(const int argc, const char **argv) {
  std::ios::sync_with_stdio(false);
  int n = 0, m = 0, sz = 0, id = 0, k = 0, c = 0, d = 0, x = 0, y = 0;
  // 表的内容获取
  std::cin >> n;  // 输入用户数目
  usertable_read();
  // 对表的操作
  std::cin >> m;
  for (int i = 1; i <= m; ++i) {  // 逐个处理
    std::cin >> s;                // 读取表达式
    sz = std::strlen(s);
    c = d = 0;
    expression_parse();
    int e = 0;
    for (int j = 1; j <= n; ++j)
      if (stk[d].test(j)) f[++e] = to[j];  // 返回 std::bitset 中位于 j 位置的值
    output_id();
  }
  return 0;
}