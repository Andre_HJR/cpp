#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
#define SZ(x) (int)x.size()
#define fi first
#define se second
const int N = 2e5 + 10, INF = 0x3f3f3f3f;
int n, m, l, r, a[N], b[N];
vector<int> L[N], R[N];
ll ans;
struct segtree {
  int n;
  struct node {
    int l, r, c, mn, mx;
  } e[N << 2];
#define l(p) e[p].l
#define r(p) e[p].r
#define c(p) e[p].c
#define mn(p) e[p].mn
#define mx(p) e[p].mx
  void up(int p) {
    mn(p) = min(mn(p << 1), mn(p << 1 | 1));
    mx(p) = max(mx(p << 1), mx(p << 1 | 1));
  }
  void bld(int p, int l, int r) {
    l(p) = l;
    r(p) = r;
    c(p) = 0;
    if (l == r) {
      mn(p) = INF;
      mx(p) = -INF;
      return;
    }
    int mid = ( l + r ) >> 1;
    bld(p << 1, l, mid);
    bld(p << 1 | 1, mid + 1, r);
    up(p);
  }
  void init(int _n) {
    n = _n;
    bld(1, 1, n);
  }
  void chg(int p, int x, int v) {
    if (l(p) == r(p)) {
      mn(p) = min(mn(p), v);
      mx(p) = max(mx(p), v);
      return;
    }
    int mid = ( l(p) + r(p) ) >> 1;
    psd(p);
    chg(p << 1 | (x > mid), x, v);
    up(p);
  }
  void psd(int p) {
    if (c(p)) {
      mn(p << 1) = INF;
      mx(p << 1) = -INF;
      c(p << 1) = c(p);
      mn(p << 1 | 1) = INF;
      mx(p << 1 | 1) = -INF;
      c(p << 1 | 1) = c(p);
      c(p) = 0;
    }
  }
  void del(int p, int ql, int qr) {
    if (ql <= l(p) && r(p) <= qr) {
      mn(p) = INF;
      mx(p) = -INF;
      c(p) = 1;
      return;
    }
    psd(p);
    int mid = ( l(p) + r(p) ) >> 1;
    if (ql <= mid) del(p << 1, ql, qr);
    if (qr > mid) del(p << 1 | 1, ql, qr);
    up(p);
  }
  int amn(int p, int ql, int qr) {
    if (ql <= l(p) && r(p) <= qr) return mn(p);
    int mid = ( l(p) + r(p) ) >> 1, res = INF;
    psd(p);
    if (ql <= mid) res = min(res, amn(p << 1, ql, qr));
    if (qr > mid) res = min(res, amn(p << 1 | 1, ql, qr));
    return res;
  }
  int amx(int p, int ql, int qr) {
    if (ql <= l(p) && r(p) <= qr) return mx(p);
    int mid = ( l(p) + r(p) ) >> 1, res = -INF;
    psd(p);
    if (ql <= mid) res = max(res, amx(p << 1, ql, qr));
    if (qr > mid) res = max(res, amx(p << 1 | 1, ql, qr));
    return res;
  }
} seg, lseg, rseg;
struct BitPre {
  int n, tr[N];
  void init(int _n) {
    n = _n;
    memset(tr, 0, (n + 1) * sizeof(*tr));
  }
  void add(int x, int v) {
    for (int i = x; i <= n; i += i & -i) tr[i] += v;
  }
  int ask(int x) {
    if (x < 0) return 0;
    int ans = 0;
    for (int i = x; i; i -= i & -i) ans += tr[i];
    return ans;
  }
} tr;
bool ok(int x) { return x != INF && x != -INF; }
bool in(int x, int l, int r) { return l <= x && x <= r; }
void cdq(int l, int r) {
  if (l == r) return;
  int mid = (l + r) / 2;
  cdq(l, mid);
  cdq(mid + 1, r);
  for (int i = mid; i >= l; --i) {
    a[i] = -INF;
    b[i] = INF;
    for (auto &v : L[i]) {
      if (v > r) continue;
      if (v <= mid)
        a[i] = max(a[i], v);
      else
        b[i] = min(b[i], v);  //有无需本侧的情况
      if (v >= mid) rseg.chg(1, v, i);
    }
    if (ok(a[i])) {
      a[i] = max(a[i], seg.amx(1, i, min(mid, a[i] + 1)));
      seg.chg(1, i, a[i]);
    }
  }
  for (int i = mid + 1; i <= r; ++i) {
    a[i] = INF;
    b[i] = -INF;
    for (auto &v : R[i]) {
      if (v < l) continue;
      if (v >= mid + 1)
        a[i] = min(a[i], v);
      else
        b[i] = max(b[i], v);
      if (v <= mid + 1) lseg.chg(1, v, i);
    }
    if (ok(a[i])) {
      a[i] = min(a[i], seg.amn(1, max(mid + 1, a[i] - 1), i));
      seg.chg(1, i, a[i]);
    }
  }
  vector<array<int, 3>> all;
  for (int i = mid; i >= l; --i) {
    if (ok(a[i])) {  // [i,a[i]+1]
      int v = lseg.amn(1, i, a[i] + 1);
      if (in(v, mid + 1, r)) {
        b[i] = min(b[i], v);
      }
    }
    if (in(b[i], mid + 1, r)) all.push_back({i, 0, b[i]});
  }
  for (int i = mid + 1; i <= r; ++i) {
    if (ok(a[i])) {  // [a[i]-1,i]
      int v = rseg.amx(1, a[i] - 1, i);
      if (in(v, l, mid)) {
        b[i] = max(b[i], v);
      }
    }
    if (in(b[i], l, mid)) all.push_back({b[i], 1, i});
  }
  sort(all.begin(), all.end());
  for (auto &w : all) {
    int op = w[1], ub = w[2];
    if (op == 0)
      tr.add(ub, 1);
    else
      ans += tr.ask(
          ub);  //左[l,a[l]]右[a[r],r]，满足l<=a[r]<=a[l]+1且a[r]-1<=a[l]<=r，a[l]<=mid<mid+1<=a[r]显然成立
  }
  seg.del(1, l, r);
  lseg.del(1, l, r);
  rseg.del(1, l, r);
  for (auto &w : all) {
    int op = w[1], ub = w[2];
    if (op == 0) tr.add(ub, -1);
  }
}
int main() {
  // scanf("%d%d",&n,&m);
  std::cin >> n >> m;
  seg.init(n);
  lseg.init(n);
  rseg.init(n);
  tr.init(n);
  for (int i = 1; i <= m; ++i) {
    // scanf("%d%d",&l,&r);//重复无所谓
    std::cin >> l >> r;
    L[l].push_back(r);
    R[r].push_back(l);
  }
  cdq(1, n);
  printf("%lld\n", ans);
  return 0;
}