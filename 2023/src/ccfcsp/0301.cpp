/**
 * @file 0301.cpp
 * @author andre HJR (3191971830@qq.com)
 * @brief 2023-03-01: 田地丈量
 * @version 0.1
 * @date 2023-04-27
 *
 * @copyright Copyright (c) 2023
 *
 * @details
 *
 *   矩形面积交 = x 轴线段交长度 * y 轴线段交长度
 *
 * 线段交长度，相交的时候 min 右端点 - max 左端点，不相交的时候是 0。
 */

#include <iostream>

int main(const int argc, const char** argv) {
  std::ios::sync_with_stdio(false);
  int n = 0, a = 0, b = 0, x_1 = 0, y_1 = 0, x_2 = 0, y_2 = 0, x_t = 0, y_t = 0,
      sum = 0;
  std::cin >> n >> a >> b;
  for (int i = 0; i < n; ++i) {
    x_1 = x_2 = y_1 = y_2 = x_t = y_t = 0;
    std::cin >> x_1 >> y_1 >> x_2 >> y_2;
    x_t = std::min(a, x_2) - std::max(0, x_1);  // 正方形矩阵相交面积的计算
    y_t = std::min(b, y_2) - std::max(0, y_1);
    if (x_t >= 0 && y_t >= 0)
      sum +=
          x_t *
          y_t;  // `==` 题目中给出， 任意两块的交集面积均为0，仅边界处可能重叠
  }
  std::cout << sum;
  return 0;
}